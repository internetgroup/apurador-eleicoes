 var request = require('request'),
	logger = require('winston');

// Propriedades usadas:
var RT_SERVER_REFRESH = 10000;
var PFX_CANAL = 'eleicoes:';
var config = null, 
	ortc_url = null;

// Controladores:

exports.iniciar = function(_config)  {
	config = _config;

	setInterval(atualizarUrlRT, RT_SERVER_REFRESH);
	atualizarUrlRT();
};

function atualizarUrlRT (callbackSucesso, callbackErro) {
	var options = {
		url: config.ortc.CLUSTER_URL,
		method: 'GET'
	}
	if (config.ortc.proxy) {
		options.proxy = config.ortc.proxy;
	}

	request(options, function (error, res, data) {
		if (error) {
			logger.error('Erro ao obter servidor ORTC: ' + config.ortc.CLUSTER_URL, error);
			if (callbackErro) callbackErro();
			return;
		}
		if (res.statusCode != 200) {
			logger.error('Erro ao obter servidor ORTC: ' + config.ortc.CLUSTER_URL, res.statusCode);
			if (callbackErro) callbackErro();
			return;
		}
		eval(data);
		ortc_url = SOCKET_SERVER;
		logger.info('Servidor ORTC obtido: ', ortc_url);
		if (callbackSucesso) callbackSucesso();
	});
}

function autenticarServerRT (canal, callbackSucesso, callbackErro) {
	var body = 'AT=' + config.ortc.AT_SERVER + '&PVT=0&AK=' + config.ortc.AK + '&TTL=' + config.ortc.AUTH_TTL + '&PK=' + config.ortc.PK +
				'&TP=1&' + PFX_CANAL + canal + '=w';
	requestRT('/authenticate', body, callbackSucesso, callbackErro);
	//callbackSucesso();
}

function autenticarClientRT () {
	var body = 'AT=' + config.ortc.AT_CLIENT + '&PVT=0&AK=' + config.ortc.AK + '&TTL=' + config.ortc.AUTH_TTL + '&PK=' + config.ortc.PK +
				'&TP=1&' + PFX_CANAL + '*=r';
	requestRT('/authenticate', body, function () {
		console.log('Cliente autenticado')
	}, function () {
		console.log('Erro ao autenticar cliente')
	});
}

function postarMensagemRT (canal, mensagem, callbackSucesso, callbackErro) {
	var body = 'AT=' + config.ortc.AT_SERVER + '&AK=' + config.ortc.AK + '&PK=' + config.ortc.PK +
				'&C=' + PFX_CANAL + canal + '&M=' + encodeURIComponent(mensagem);
	requestRT('/send', body, callbackSucesso, callbackErro);
}

exports.postarMensagemRT = postarMensagemRT;

function requestRT (path, body, callbackSucesso, callbackErro) {
	if (!ortc_url) {
		atualizarUrlRT(function () {
			requestRT(path, body, callbackSucesso, callbackErro)
		}, callbackErro);
		return;
	}
	var options = {
		url: ortc_url + path,
		method: 'POST',
		body: body
	}
	logger.info('Posting to ' + options.url + ' ' + body);

	if (config.ortc.proxy) {
		options.proxy = config.ortc.proxy;
	}
	request(options, function(error, res, data) {
		if (error) {
			var msg = error + ' for ' + options.url;
			logger.error(msg);
			if (callbackErro) callbackErro(msg);
		  	return;
		}

		if (res.statusCode != 201) {
			var msg = 'Status ' + res.statusCode + ' for ' + options.url;
			logger.error(msg);
		  	if (callbackErro) callbackErro(res);
		  	return;
		}

		logger.info('Status ' + res.statusCode + ' for ' + options.url);
		if (callbackSucesso) callbackSucesso();
	});
}
