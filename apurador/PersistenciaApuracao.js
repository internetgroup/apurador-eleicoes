/*
 * Classe que lida com os documentos da apuracao em um mecanismo de persistencia
 */

var COLECAO_INDICE = 'indice',
	COLECAO_FIXO_ABRANGENCIA = 'fixoAbrangencia'

function PersistenciaApuracao (enginePersistencia, instancia) {
	this._enginePersistencia = enginePersistencia
	this._instancia = instancia;
	this._colecaoIndice = this._instancia + '_' + COLECAO_INDICE;
	this._colecaoFixoAbrangencia = this._instancia + '_' + COLECAO_FIXO_ABRANGENCIA;
}

PersistenciaApuracao.prototype.obterIndice = function (abrangencia, onsucess, onfailure) {
	this._enginePersistencia.findOne(this._colecaoIndice, { _id: abrangencia },
		onsucess, onfailure)
}

PersistenciaApuracao.prototype.salvarIndice = function (indice, onsucess, onfailure) {
	this._enginePersistencia.save(this._colecaoIndice, indice,
		onsucess, onfailure)
}

PersistenciaApuracao.prototype.obterFixoAbrangencia = function (nome, onsucess, onfailure) {
	this._enginePersistencia.findOne(this._colecaoFixoAbrangencia, { _id: nome },
		onsucess, onfailure)
}

PersistenciaApuracao.prototype.salvarFixoAbrangencia = function (municipioFixo, onsucess, onfailure) {
	this._enginePersistencia.save(this._colecaoFixoAbrangencia, municipioFixo,
		onsucess, onfailure)
}

exports.PersistenciaApuracao = PersistenciaApuracao;