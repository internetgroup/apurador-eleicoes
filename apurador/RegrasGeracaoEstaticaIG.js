var utils = require('./utils.js').utils;

var ARQUIVO_PRESIDENTE = 'presidente',
	ARQUIVO_GOVERNADOR = 'governador',
	ARQUIVO_SENADOR    = 'senador',
	ARQUIVO_DEPUTADO_FEDERAL    = 'deputado-federal',
	ARQUIVO_DEPUTADO_ESTADUAL   = 'deputado-estadual',
	ARQUIVO_DEPUTADO_DISTRITAL  = 'deputado-distrital',
	ARQUIVO_PREFEITOS  = 'prefeito',
	ARQUIVO_VEREADORES = 'vereador',
	ARQUIVO_TOTAIS_MUNICIPIO = 'resultado-prefeito',
	ARQUIVO_PREFEITO_HOME    = 'home-prefeitos',
	ARQUIVO_TOTAIS_BR        = 'home-totais',
	EXT_INCLUDE = '.shtml',
	EXT_JSON    = '.json';

function RegrasGeracaoEstaticaIG (config) {
	this._config = config;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoIncludeTotaisBr = function (dadosFixos) {
	return this._obterCaminhoIncludesHome(dadosFixos) + '/' + ARQUIVO_TOTAIS_BR + EXT_INCLUDE;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoIncludePrefeito = function (dadosFixos) {
	return this._obterCaminhoIncludesMunicipio(dadosFixos) + '/' + ARQUIVO_PREFEITOS + EXT_INCLUDE;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoIncludePrefeitoHome = function (dadosFixos) {
	return this._obterCaminhoIncludesHome(dadosFixos) + '/' + ARQUIVO_PREFEITO_HOME + 
			'-' + dadosFixos.municipio.uf + EXT_INCLUDE;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoIncludeTotaisMunicipio = function (dadosFixos) {
	return this._obterCaminhoIncludesMunicipio(dadosFixos) + '/' + ARQUIVO_TOTAIS_MUNICIPIO + EXT_INCLUDE;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoIncludeVereador = function (dadosFixos) {
	return this._obterCaminhoIncludesMunicipio(dadosFixos) + '/' + ARQUIVO_VEREADORES + EXT_INCLUDE;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoJsonTotaisBr = function (dadosFixos) {
	return this._obterCaminhoIncludesHome(dadosFixos) + '/' +
			 ARQUIVO_TOTAIS_BR + '-' + new Date().getTime() + EXT_JSON;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoJsonPrefeito = function (dadosFixos) {
	return this._obterCaminhoIncludesMunicipio(dadosFixos) + '/' + ARQUIVO_PREFEITOS +
			'-' + new Date().getTime() + EXT_JSON;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoJsonPrefeitoHome = function (dadosFixos) {
	return this._obterCaminhoIncludesHome(dadosFixos) + '/' + ARQUIVO_PREFEITO_HOME +
			'-' + dadosFixos.municipio.uf + EXT_JSON;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoJsonTotaisMunicipio = function (dadosFixos) {
	return this._obterCaminhoIncludesMunicipio(dadosFixos) + '/' + ARQUIVO_TOTAIS_MUNICIPIO +
			'-' + new Date().getTime() + EXT_JSON;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoJsonVereador = function (dadosFixos) {
	return this._obterCaminhoIncludesMunicipio(dadosFixos) + '/' + ARQUIVO_VEREADORES +
			'-' + new Date().getTime() + EXT_JSON;
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoPaginaMunicipio = function (dadosFixos) {
	return ('/' + this._config.raizURLMunicipios + '/' + 
			this._obterCaminhoIncludesMunicipio(dadosFixos) + '/').replace(/\/+/g, '/');
}

RegrasGeracaoEstaticaIG.prototype.obterCaminhoHomeEleicoes = function (dadosFixos) {
	return this._config.caminhoHome;
}

RegrasGeracaoEstaticaIG.prototype.obterURLPaginaMunicipio = function (dadosFixos) {
	return this._config.urlBase.replace(/\/$/, '') + this.obterCaminhoPaginaMunicipio(dadosFixos);
}

RegrasGeracaoEstaticaIG.prototype.obterURLHomeEleicoes = function (dadosFixos) {
	return this._config.urlBase.replace(/\/$/, '') + this.obterCaminhoHomeEleicoes(dadosFixos);
}

RegrasGeracaoEstaticaIG.prototype._obterCaminhoIncludesMunicipio = function (dadosFixos) {
	var municipio;
	for (var codMunicipio in dadosFixos.municipios)
		municipio = dadosFixos.municipios[codMunicipio];
	
	return municipio.uf + '/' + utils.normalizarNome(municipio.nome);	
}

RegrasGeracaoEstaticaIG.prototype._obterCaminhoIncludesHome = function (dadosFixos) {
	return '';	
}

exports.RegrasGeracaoEstaticaIG = RegrasGeracaoEstaticaIG;