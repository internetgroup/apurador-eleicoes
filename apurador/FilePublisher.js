var fs = require('fs'),
	logger = require('winston');

function FilePublisher () {
}

FilePublisher.prototype.publish = function (caminho, conteudo, cb) {
	logger.info('Publishing to file', caminho);
	try {
		FilePublisher.ensurePath(caminho.substring(0, caminho.lastIndexOf('/')));
		logger.debug('Writing temp file ' + caminho + '.tmp')
		fs.writeFile(caminho + '.tmp', conteudo, function (err) {
			if (err) {
				var msg = 'Failed to write to path ' + caminho;
				logger.error(msg)
				cb(new Error(msg))
				return;				
			}
			logger.debug('Moving temp file to ' + caminho);
			fs.rename(caminho + '.tmp', caminho, function (err) {
				if (err) {
					var msg = 'Failed to rename ' + caminho + '.tmp to ' + caminho + ': ' + err;
					logger.error(msg)
					cb(new Error(msg));
					return;													
				}
				cb();
			})
		})

	} catch (e) {
		var msg = 'Failed to write to path ' + caminho + ': ' + e;
		logger.error(msg)
		cb(new Error(msg));
	}
}

FilePublisher.ensurePath = function (path, root) {
    var dirs = path.replace(/\/+/g, '/').split('/'), dir = dirs.shift(), root = (root||'')+dir+'/';

    try { fs.mkdirSync(root); }
    catch (e) {
        //dir wasn't made, something went wrong
        if(!fs.statSync(root).isDirectory()) throw new Error(e);
    }

    return !dirs.length||FilePublisher.ensurePath(dirs.join('/'), root);
}

module.exports = FilePublisher;