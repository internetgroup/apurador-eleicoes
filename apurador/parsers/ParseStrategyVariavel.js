function ParseStrategyVariavel () {}		

ParseStrategyVariavel.configureParser = function (parser) {
	var resultado = null,
		abrangencias = [],
		refAbrangenciaAtual = null,
		done = false;

	parser.addListener('startElement', function (name, attrs) {
		if (done) return;

		if (name == 'Resultado') {
			resultado = attrs;

		} else if (name == 'Abrangencia') {
			refAbrangenciaAtual = attrs;
			refAbrangenciaAtual.votosPartido = [];
			refAbrangenciaAtual.votosCandidato = [];
			abrangencias.push(refAbrangenciaAtual);

		} else if (name == 'VotoPartido') {
			if (refAbrangenciaAtual === null) {
				throw new Error('Elemento "VotoPartido" fora de um elemento "Abrangencia"');
			}
			refAbrangenciaAtual.votosPartido.push(attrs);

		} else if (name == 'VotoCandidato') {
			if (refAbrangenciaAtual === null) {
				throw new Error('Elemento "VotoCandidato" fora de um elemento "Abrangencia"');
			}
			refAbrangenciaAtual.votosCandidato.push(attrs);
		}
	})	

	parser.addListener('endElement', function (name) {
		if (name == 'Br') {
			br = null;

		} else if (name == 'Abrangencia') {
			refAbrangenciaAtual = null;
		}
	})		

	return function (cb) {
		cb({
			resultado: resultado,
			abrangencias: abrangencias,
		})
	}
}
		
module.exports = ParseStrategyVariavel;		