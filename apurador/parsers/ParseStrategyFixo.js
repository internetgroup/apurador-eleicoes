function ParseStrategyFixo () {}		

ParseStrategyFixo.configureParser = function (parser) {
	var dadosFixos = null,
		br = null,
		ufs = [],
		refUFAtual = null,
		cargo = {},
		partidos = {},
		candidatos = {},
		numeroPartidoAtual = null,
		refCandidatoAtual = null,
		done = false;

	parser.addListener('startElement', function (name, attrs) {
		if (done) return;

		if (name == 'DadosFixos') {
			dadosFixos = attrs;

		} else if (name == 'Br') {
			br = attrs;	

		} else if (name == 'Uf') {
			if (br === null) {
				throw new Error('Elemento "Uf" fora de um elemento "Br"');
			}
			refUFAtual = attrs;
			refUFAtual.municipios = [];
			ufs.push(refUFAtual);

		} else if (name == 'Municipio') {
			if (refUFAtual === null) {
				throw new Error('Elemento "Municipio" fora de um elemento "Uf"');
			}
			refUFAtual.municipios.push(attrs);

		} else if (name == 'Cargo') {
			cargo = attrs;
			partidos = {};
			candidatos = {};

		} else if (name == 'Partido') {
			if (cargo === null) {
				throw new Error('Elemento "Partido" fora de um elemento "Cargo"');
			}
			partidos[attrs.numero] = attrs;
			numeroPartidoAtual = attrs.numero;

		} else if (name == 'Candidato') {
			if (numeroPartidoAtual === null) {
				throw new Error('Elemento "Candidato" fora de um elemento "Partido"');
			}
			refCandidatoAtual = candidatos[attrs.numero] = attrs;
			refCandidatoAtual.numeroPartido = numeroPartidoAtual;
		
		} else if (name == 'ViceSuplente') {
			if (refCandidatoAtual === null) {
				throw new Error('Elemento "ViceSuplente" fora de um elemento "Candidato"');
			}
			refCandidatoAtual.viceSuplente = attrs;

		} else if (name == 'Substituido') {
			if (refCandidatoAtual === null) {
				throw new Error('Elemento "Substituido" fora de um elemento "Candidato"');
			}
			refCandidatoAtual.substituto = attrs;
		}
	})	

	parser.addListener('endElement', function (name) {
		if (name == 'Uf') {
			refUFAtual = null;

		} else if (name == 'Cargo') {
			cargo = null;

		} else if (name == 'Partido') {
			numeroPartidoAtual = null;

		} else if (name == 'Candidato') {
			refCandidatoAtual = null;
		}
	})		

	return function (cb) {
		cb({
			dadosFixos: dadosFixos,
			br: br,
			cargo: cargo,
			partidos: partidos,
			candidatos: candidatos
		});
	}
}
		
module.exports = ParseStrategyFixo;		