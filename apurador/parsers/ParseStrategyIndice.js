var logger = require('winston');
var Classificador = require('../ClassificadorArquivos')

function ParseStrategyIndice () {}		

var REGEX_DATA_HORA = /^(\d\d)\/(\d\d)\/(\d\d\d\d) (\d\d):(\d\d):(\d\d)$/;

ParseStrategyIndice.configureParser = function (parser) {
	var variaveis = {},
		fixos = {},
		done = false;

	parser.addListener('startElement', function (name, attrs) {
		if (name == 'ArquivoIndice') {
			var dt = REGEX_DATA_HORA.exec(attrs.dataHora);
			for (var i = 1; i < dt.length; i++)
				dt[i] = parseInt(dt[i], 10)
			d = new Date(dt[3], dt[2] - 1, dt[1], dt[4], dt[5], dt[6]);
			var nome = attrs.nome.substring(0, attrs.nome.length - 4);	// Removendo extensao

			var classificacao = Classificador.classificar(nome);

			switch (classificacao.tipo) {
				case 'fixo':
					fixos[nome] = d.getTime();
					break;

				case 'variavel':
					variaveis[nome] = d.getTime();
					break;

				default:
					logger.info('Ignorando arquivo', nome, '- tipo de arquivo nao reconhecido');
			}
		}
	})	

	return function (cb) {
		cb({
			variaveis: variaveis,
			fixos: fixos
		})
	};
}
		
module.exports = ParseStrategyIndice;		