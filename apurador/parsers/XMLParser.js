var expat = require('node-expat');

function XMLParser () {}

var XML_READ_CHUNK_SIZE = 4096;

XMLParser.parse = function (xml, strategy, onsucess, onfailure) {
	var parser = new expat.Parser('UTF-8');

	var cb = strategy.configureParser(parser);

	parseXML(xml, parser, 
		function () {
			cb(onsucess);
		},
		onfailure);	
}

function parseXML (xml, parser, onsucess, onfailure) {
	var buffer = new Buffer(xml, 'utf8');
	for (var i = 0; i < buffer.length; i += XML_READ_CHUNK_SIZE) {
		//logger.debug('Reading from position ' + i + ' at ' + nomeXML)
		var end = i + XML_READ_CHUNK_SIZE;
		if (end > buffer.length) end = buffer.length;
		if (!parser.parse(buffer.slice(i, end), false)) {
			var msg = 'Could not parse file - corrupted file?';
			logger.error(msg);
			onfailure(msg);
			return;
		}
	}
	onsucess();	
}

function failWithMessage (msg, onfailure) {
	logger.error(msg);
	onfailure(msg)
}

module.exports = XMLParser;
