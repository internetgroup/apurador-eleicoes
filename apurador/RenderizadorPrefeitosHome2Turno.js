var utils = require('./utils.js').utils,
    logger = require('winston');

function RenderizadorPrefeitosHome (regras) {
	this._regras = regras;
}

RenderizadorPrefeitosHome.prototype.aplicavel = function (dadosFixos) {
	return dadosFixos.cargo == '0011' && dadosFixos.municipio.capital == 'S';
}

RenderizadorPrefeitosHome.prototype.renderizar = function (arquivoFixo, arquivoVariavel) {
    // console.log('boxPrefeitoHome');
    this._result = null;

    var candidatos = arquivoFixo.candidatos,
        partidos = arquivoFixo.partidos,
        candidatosVar = arquivoVariavel.candidatos,
        porcentagem = ((arquivoVariavel.stats.eleitoradoApurado*100) / arquivoFixo.municipio.eleitorado).toFixed(2),
        porcentagem_brancos_nulos = (parseFloat(arquivoVariavel.stats.votosEmBranco) / arquivoFixo.municipio.eleitorado * 100).toFixed(2)
        realtime = {}

    //Realtime
    if(arquivoVariavel.matematicamenteDefinido && arquivoVariavel.matematicamenteDefinido == 'E'){
        ha_eleito = true;
    }else{
        ha_eleito = false;
    }

    realtime_eleito = true
    realtime = {
        eleito: ha_eleito,
        cidade : arquivoFixo.municipio.nome,
        uf: arquivoFixo.municipio.uf,
        percentual_apurado : porcentagem,
        brancos_e_nulos : porcentagem_brancos_nulos,
        candidatos : []
    }

    var html = '\
        <div id="uf-' + arquivoFixo.municipio.uf + '">\
        <div class="header-capital">\
            <h4 class="nome-cidade">' + arquivoFixo.municipio.nome + '</h4>\
            <div class="status-apuracao">\
                <span class="porcentagem">' + porcentagem + '%</span><small>de votos<br>apurados</small><br>\
            </div>\
            <div class="clear"></div>\
        </div>';

    for (var i=0; i<candidatos.length && i < 2; i++) {

        if(candidatosVar[candidatos[i].numero].eleito && candidatosVar[candidatos[i].numero].eleito == 'S'){
            ha_eleito = true;
        }

        //percentual candidato
        porcentagem_candidato = ((parseFloat(candidatosVar[candidatos[i].numero].totalVotos) / parseFloat(arquivoVariavel.stats.votosValidos)) * 100).toFixed(2);
        porcentagem_candidato = isNaN(porcentagem_candidato) ? "0.00" : porcentagem_candidato
        porcentagem_candidato = porcentagem_candidato + ""

        //quebra float para adequacao layout
        porcentagem_int = porcentagem_candidato.split(".")[0]
        porcentagem_dec = porcentagem_candidato.split(".")[1]

        //json para disparo realtime
        realtime['candidatos'].push({
            nome : candidatos[i].nomeUrna,
            partido : partidos[candidatos[i].partido].sigla,
            porcentagem : porcentagem_candidato,
            votos : utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos)
        });

        cand_class = ""; //classes extras para primeiro colocado
        selo_eleito = "" //html de selo de eleito
        if(i==0){
            cand_class = "primeiro"
            if(ha_eleito){
                cand_class += " eleito"
                selo_eleito = '<span class="selo-eleito">ELEITO</span>'
            }
        }

        html += '\
            <div class="candidato '+partidos[candidatos[i].partido].sigla.toLowerCase()+' '+cand_class+'">\
                <img class="thumb" src="http://i0.ig.com/ultimosegundo/eleicoes/2012/fotos-2turno/'+ utils.normalizarNome(arquivoFixo.municipio.nome) +'-'+ partidos[candidatos[i].partido].sigla.toLowerCase() +'.jpg" />\
                <div class="nome-partido">\
                    '+selo_eleito+'\
                    <h3>'+ candidatos[i].nomeUrna + '</h3>\
                    <h4>'+ partidos[candidatos[i].partido].sigla + '</h4>\
                </div>\
                <div class="stats">\
                    <big>'+ utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) + '</big>\
                    <small>votos válidos</small>\
                </div>\
                <div class="percent">\
                    <h4>\
                        <big>'+porcentagem_int+',</big>\
                        <small>'+porcentagem_dec+'</small>\
                        <span>%</span>\
                    </h4>\
                </div>\
            </div>';
    }

    html += '</div>';

    realtime.eleito = ha_eleito;

    this._result = {json: 'apuracao_home_callback(' + JSON.stringify(realtime) + ')', html: html};
    return true;

}

RenderizadorPrefeitosHome.prototype.getDescricao = function () {
	return 'Box de prefeitos'
}

RenderizadorPrefeitosHome.prototype.getURLPaginaContainer = function (dadosFixos) {
  return [ { url: this._regras.obterURLHomeEleicoes(dadosFixos), seletor: '#uf-' + dadosFixos.municipio.uf } ];
}

RenderizadorPrefeitosHome.prototype.obterCaminhoGravacao = function (dadosFixos, formato) {
    if (formato == 'html')
        return this._regras.obterCaminhoIncludePrefeitoHome(dadosFixos);

    if (formato == 'json')
        return this._regras.obterCaminhoJsonPrefeitoHome(dadosFixos);

    msg = 'No render result for format ' + formato;
    logger.error(msg);
    throw new Error(msg);
}

exports.RenderizadorPrefeitosHome = RenderizadorPrefeitosHome;