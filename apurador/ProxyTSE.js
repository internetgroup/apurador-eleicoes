var express = require('express'),
	logger = require('winston'),
	FachadaXMLApuracao = require ('./FachadaXMLApuracao.js').FachadaXMLApuracao,
	FachadaJsonApuracaoIG = require ('./FachadaJsonApuracaoIG.js').FachadaJsonApuracaoIG;	

var URL_PREFEITO = 'pre', 
	URL_VEREADOR = 'ver',
	URL_PRESIDENTE = 'pres', 
	URL_GOVERNADOR = 'gov',
	URL_SENADOR = 'sen',
	URL_DEPUTADO_FEDERAL = 'depf',
	URL_DEPUTADO_ESTADUAL = 'depe',
	URL_DEPUTADO_DISTRITAL = 'depd';

/*
 * Quando instanciada e inicializada, essa classe sobe um servidor de proxy para os dados do TSE.
 * Este servidor esconde a complexidade das regras de URL do servidor do TSE e automaticamente descompacta
 * e serve os XMLs dos arquivos Zip
 */

function ProxyTSE (config) {
	this._config = config;
	this._fachadaXML = new FachadaXMLApuracao(config);
	this._fachadaJson = new FachadaJsonApuracaoIG(config);
}

ProxyTSE.prototype.iniciar = function () {
	return;
	var app = express() // express.createServer(), //---
		self = this;

	app.get('/xml/var/:uf/:municipioID/:cargo', function (req, res) {
		self._fachadaXML.obterVariavelMunicipio(req.params.uf, req.params.municipioID, traduzirCargo(req.params.cargo), 
			criarResponseWriterXML(res), criarResponseWriterErro(res))
	})

	app.get('/xml/fixo/:uf/:municipioID/:cargo/:versao', function (req, res) {
		self._fachadaXML.obterFixoMunicipio(req.params.uf, req.params.municipioID, 
			traduzirCargo(req.params.cargo), req.params.versao,
			criarResponseWriterXML(res), criarResponseWriterErro(res))
	})

	app.get('/xml/var/:abrangencia', function (req, res) {
		self._fachadaXML.obterVariavelAbrangencia(req.params.abrangencia, 
			criarResponseWriterXML(res), criarResponseWriterErro(res))
	})

	app.get('/xml/fixo/:abrangencia/:versao', function (req, res) {
		self._fachadaXML.obterFixoAbrangencia(req.params.abrangencia, req.params.versao,
			criarResponseWriterXML(res), criarResponseWriterErro(res))
	})

	app.get('/xml/ind/:abrangencia', function (req, res) {
		self._fachadaXML.obterIndice(req.params.abrangencia, 
			criarResponseWriterXML(res), criarResponseWriterErro(res))
	})

	app.get('/xml/conf/:abrangencia', function (req, res) {
		self._fachadaXML.obterConfigMunicipios(req.params.abrangencia, 
			criarResponseWriterXML(res), criarResponseWriterErro(res))
	})

	app.get('/xml/:uf/:nomeArquivo', function (req, res) {
		self._fachadaXML.obterZipDentroAbrangencia(req.params.uf, req.params.nomeArquivo, 
			criarResponseWriterBinary(res), criarResponseWriterErro(res))
	})

	app.get('/json/var/br', function (req, res) {
		self._fachadaJson.obterVariavelBR(criarResponseWriterJson(res), criarResponseWriterErro(res))
	})

	app.get('/json/fixo/br/:versao', function (req, res) {
		self._fachadaJson.obterFixoBR(req.params.versao,
			criarResponseWriterJson(res), criarResponseWriterErro(res))
	})

	app.get('/json/var/:uf/:municipioID/:cargo', function (req, res) {
		self._fachadaJson.obterVariavelMunicipio(req.params.uf, req.params.municipioID, traduzirCargo(req.params.cargo), 
			criarResponseWriterJson(res), criarResponseWriterErro(res))
	})

	app.get('/json/fixo/:uf/:municipioID/:cargo/:versao', function (req, res) {
		self._fachadaJson.obterFixoMunicipio(req.params.uf, req.params.municipioID, 
			traduzirCargo(req.params.cargo), req.params.versao,
			criarResponseWriterJson(res), criarResponseWriterErro(res))
	})

	app.get('/json/ind/:abrangencia', function (req, res) {
		self._fachadaJson.obterIndice(req.params.abrangencia, 
			criarResponseWriterXML(res), criarResponseWriterErro(res))
	})	

	app.listen(this._config.portaProxyTSE);
	logger.info('Proxy to TSE started on port ' + this._config.portaProxyTSE);
}

function traduzirCargo (cargoURL) {
	return ( cargoURL == URL_PREFEITO ? FachadaXMLApuracao.cargo.PREFEITO :
					cargoURL == URL_VEREADOR ? FachadaXMLApuracao.cargo.VEREADOR :
					cargoURL == URL_PRESIDENTE ? FachadaXMLApuracao.cargo.PRESIDENTE :
					cargoURL == URL_GOVERNADOR ? FachadaXMLApuracao.cargo.GOVERNADOR :
					cargoURL == URL_SENADOR ? FachadaXMLApuracao.cargo.SENADOR :
					cargoURL == URL_DEPUTADO_FEDERAL ? FachadaXMLApuracao.cargo.DEPUTADO_FEDERAL :
					cargoURL == URL_DEPUTADO_ESTADUAL ? FachadaXMLApuracao.cargo.DEPUTADO_ESTADUAL :
					cargoURL == URL_DEPUTADO_DISTRITAL ? FachadaXMLApuracao.cargo.DEPUTADO_DISTRITAL :
					cargoURL );	
}

function criarResponseWriterXML (res) {
	return function (xml) {
		res.status(200);
		res.header('Content-Type', 'application/xml; charset=UTF-8');
		res.send(xml);
	}
}

function criarResponseWriterJson (res) {
	return function (json) {
		res.status(200);
		res.header('Content-Type', 'application/json; charset=UTF-8');
		res.send(JSON.stringify(json));
	}
}

function criarResponseWriterBinary (res) {
	return function (buffer) {
		res.status(200);
		//res.header('Content-Type', 'application/json; charset=UTF-8');
		res.send(buffer);
	}
}

function criarResponseWriterErro (res) {
	return function (msgErro) {
		res.status(500);
		res.send(msgErro);
	}
}

exports.ProxyTSE = ProxyTSE;