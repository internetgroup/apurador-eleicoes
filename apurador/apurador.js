var FachadaXMLApuracao = require ('./FachadaXMLApuracao.js').FachadaXMLApuracao,
	FachadaJsonApuracaoIG = require ('./FachadaJsonApuracaoIG.js').FachadaJsonApuracaoIG,
	ProxyTSE = require('./ProxyTSE.js').ProxyTSE,
    Classificador = require('./ClassificadorArquivos'),
	MongoPersistence = require('./MongoPersistence.js').MongoPersistence,
	PersistenciaApuracao = require('./PersistenciaApuracao').PersistenciaApuracao,
	RegrasGeracaoEstaticaIG = require('./RegrasGeracaoEstaticaIG').RegrasGeracaoEstaticaIG,
	RenderizadorTabela = require('./RenderizadorTabela').RenderizadorTabela,
	RenderizadorTotaisMunicipio = require('./RenderizadorTotaisMunicipio').RenderizadorTotaisMunicipio,		
	RenderizadorPrefeitosHome = require('./RenderizadorPrefeitosHome2Turno').RenderizadorPrefeitosHome,
	RenderizadorTotaisBr = require('./RenderizadorTotaisBr').RenderizadorTotaisBr,
	IncludeBasedConsumerStrategy = require('./consumidores/IncludeBasedConsumerStrategy'),
	IncludePublisher = require('./2014/IncludePublisher'),
	CMSIncludeFactory = require('./2014/2-turno/CMSIncludeFactory'),
	FilePublisher = require('./FilePublisher'),
	CMSCacheLoader = require('./consumidores/includeProcessors/CMSCacheLoader');
	logger = require('winston'),
	request = require('request'),
	async = require('async'),
	winston = logger;

var config = require('./' + process.argv[2] + '/apurador-cfg.js').config;

if (process.argv[3]) {
	config.abrangencias = process.argv[3].split(',');
}

/*
 * TODO
 * Fazer geracao do arquivo de total do Brasil;
 * Escrever em arquivos temporarios;
 * [OK] Tratar uncaught exceptions; - inserido logo abaixo em 19/08/2014
 * Implementar timeout nas requisicoes HTTP. *** revisar
 * Verificar campo divulgaVotacao
 */

logger.remove(winston.transports.Console);
logger.add(winston.transports.Console, {timestamp: true, colorize: true});

process.nextTick(function memory () {
	logger.debug('Memory allocation: ', Math.round(process.memoryUsage().rss / (1024 * 1024)) + 'MB');
	setTimeout(memory, 10000);
});

var MAX_UNSAVED_ENTRIES = 20;	// A cada quantos arquivos de dados de municipio atualizados no banco para uma
								// UF o indice dessa UF deve ser atualizado no banco

var STATUS_SETUP = 1,			// Configuracao de conexoes
	STATUS_CHECKING = 2,		// Verificando atualizacoes em arquivos de dados fixos e variaveis
	STATUS_PROCESSING = 3,		// Processando os arquivos atualizados
	STATUS_IDLE = 4;			// Aguardando o tempo da proxima iteracao

var regrasRenderizacao = new RegrasGeracaoEstaticaIG(config);

var renderizadores = {
	municipio: [
		// { logica: new RenderizadorTabela(regrasRenderizacao), formatos: [ 'html' ] },
		// { logica: new RenderizadorTotaisMunicipio(regrasRenderizacao), formatos: [ 'html' ] },
		// { logica: new RenderizadorPrefeitosHome(regrasRenderizacao), formatos: [ 'html', 'json' ] }
	],
	uf: [],
	br: [
		// { logica: new RenderizadorTotaisBr(regrasRenderizacao), formatos: [ 'html' ] }
	]
}

var filePublisher = new FilePublisher();

var cacheLoader = new CMSCacheLoader(config.cacheServers);

var includePublisher = new IncludePublisher(config.publishDestination, config.baseUrl, cacheLoader);

var consumingStrategy = new IncludeBasedConsumerStrategy(
								new CMSIncludeFactory(),
								includePublisher);

var status = STATUS_SETUP;
var indice = {};			// chave: UF, valor: mapa de data de atualizacao dos arquivos da UF
var indiceAndamento = {}	// indice ainda nao completamente processado
var abrangenciasComAtualizacao = [];
var atualizacao = {			// arquivos de dados atualizados desde a ultima verificacao
	variaveis: [],
	fixos: []
};	
// chave: UF, valor: mapa de quantos arquivos de indice atualizados foram recuperados
// mas cuja informacao de data do indice ainda nao foi persistida. Quando essa quantidade chegar
// a MAX_UNSAVED_ENTRIES para alguma UF, o indice dessa UF eh persistido e o valor nesse mapa volta a zero.
var contagemAPersistir = {};	
var iteracaoAtrasada = false;	// Indice se a iteracao atual esta levando mais tempo do que o intervalo entre as iteracoes 

var agora;

// Fase de setup:
var fachadaXML = new FachadaXMLApuracao(config);
var fachadaJson = new FachadaJsonApuracaoIG(config);
var proxyTSE = new ProxyTSE(config).iniciar();
var enginePersistencia = new MongoPersistence(config);
var persistencia = null;

logger.info('Waiting consuming strategy to be ready...')
consumingStrategy.once('ready', function () {
	logger.info('Consuming strategy ready')
	enginePersistencia.init(
		function () {
			persistencia = new PersistenciaApuracao(enginePersistencia, config.instanciaID);
			carregarIndices(function () {
				iniciarCiclo();
			}, 
			encerrarComErroFatal);
		},
		encerrarComErroFatal);	
})

function carregarIndices (onsucess, onfailure) {
	logger.info('Loading file indexes from db')
	async.forEachSeries(config.abrangencias, function (abrangencia, next) {
		logger.info('Loading last known index for ' + abrangencia)
		persistencia.obterIndice(abrangencia, function (indiceAbrangencia) {
				indice[abrangencia] = indiceAbrangencia ? indiceAbrangencia : 
					{ _id: abrangencia, variaveis: {}, fixos: {} };
				next();
			},
			onfailure)
	}, onsucess)
}

function iniciarCiclo () {
	assertStatus(STATUS_SETUP);

	setInterval(function () {
		if (status == STATUS_CHECKING || status == STATUS_PROCESSING) {
			iteracaoAtrasada = true;
			logger.warn('Previous iteration is still active - waiting for it to complete')
			return;
		}
		avancarStatus();
	}, config.intervaloVerificacao);
	avancarStatus();
}

function assertStatus (expectedStatus) {
	if (status != expectedStatus) {
		var msg = 'Current status ' + status + ' does not support this operation';
		logger.error(msg);
		throw new Error(msg);
	}
}

function entrarStatusChecking () {
	indiceAndamento = {};
	atualizacao = {			
		variaveis: [],
		fixos: []
	};	
	contagemAPersistir = {};	
	abrangenciasComAtualizacao = [];
	var dt = new Date();
	agora = dt.getFullYear() + padZero(dt.getMonth() + 1) + padZero(dt.getDate()) +
				padZero(dt.getHours()) + padZero(dt.getMinutes()) + padZero(dt.getSeconds());	

	for (var i = 0; i < config.abrangencias.length; i++)
		contagemAPersistir[config.abrangencias[i]] = 0;

	async.waterfall([
		checarAtualizacoes,
		compararIndices,
		// fazerBackupIndices,
		// fazerBackupAtualizados,
		function () {
			assertStatus(STATUS_CHECKING);
			avancarStatus();			
		}
	])
}

function entrarStatusProcessing () {
	async.waterfall([
		atualizarFixos,
		atualizarVariaveis,
		function () {
			assertStatus(STATUS_PROCESSING);
			avancarStatus()
		}
	]);
}

function entrarStatusIdle () {
	logger.info('Waiting timer to start next iteration')
}

function avancarStatus () {
	switch (status) {
		case STATUS_SETUP:
		case STATUS_IDLE:
			status = STATUS_CHECKING;
			entrarStatusChecking();
			break;

		case STATUS_CHECKING:
			status = STATUS_PROCESSING;
			entrarStatusProcessing();
			break;

		case STATUS_PROCESSING:
			if (iteracaoAtrasada) {
				iteracaoAtrasada = false;
				status = STATUS_CHECKING;
				entrarStatusChecking();
				return;
			}
			status = STATUS_IDLE;
			entrarStatusIdle();
			break;
	}

}

function mudarStatus (novoStatus) {
	switch (novoStatus) {
		case STATUS_CHECKING:
			rodarIteracao();
	}
} 

function checarAtualizacoes (callback) {
	logger.info('Checking for updated on index files')
	async.forEachSeries(config.abrangencias, function (abrangencia, next) {
			logger.info('Checking updates on ' + abrangencia)
			fachadaJson.obterIndice(abrangencia, function (novoIndice) {
				indiceAndamento[abrangencia] = novoIndice;
				next();
			}, function () {
				logger.info('Skipping ' + abrangencia + ' on this iteration due to previous failure');
				next();
			})
	}, callback)
}

function compararIndices (callback) {
	for (var i = 0; i < config.abrangencias.length; i++) {
		var abrangencia = config.abrangencias[i],
			atualizacoes = 0,
			atual = indice[abrangencia],
			novo = indiceAndamento[abrangencia];

		if (!novo) {
			logger.warn('Index update not found for location ' + abrangencia + ' - skipping')
			continue;
		}

		//var limiteCache = new Date().getTime() - 30 * 60 * 1000;

		for (var arquivo in novo.variaveis) {
			// TODO: Extract this code to another class
			var classificacao = Classificador.classificar(arquivo);
			if (config.ignorarTodosMunicipios && classificacao.tipoAbrangencia == 'municipio')
				continue;
			if (config.apenasMunicipios && !config.apenasMunicipios[arquivo.substr(2, 5)])
				continue;
			else if (config.ignorarMunicipios && config.ignorarMunicipios[arquivo.substr(2, 5)]) 
				continue;

			if (!atual.variaveis[arquivo] || atual.variaveis[arquivo] < novo.variaveis[arquivo]) {
					// atual.variaveis[arquivo] < limiteCache) {
				atualizacao.variaveis.push(arquivo)
				atualizacoes++;
			}
		}

		for (var arquivo in novo.fixos) {
			// TODO: Extract this code to another class
			// if (arquivo == 'sp-0007-e044641-f001')
			// 	continue;
			if (config.apenasMunicipios && !config.apenasMunicipios[arquivo.substr(2, 5)])
				continue;
			else if (config.ignorarMunicipios && config.ignorarMunicipios[arquivo.substr(2, 5)]) 
				continue;

			if (!atual.fixos[arquivo] || atual.fixos[arquivo] < novo.fixos[arquivo]) { // ||
					//atual.fixos[arquivo] < limiteCache) {
				atualizacao.fixos.push(arquivo)
				atualizacoes++;
			}
		}
		logger.info('Found updates on ' + atualizacoes + ' files on ' + abrangencia)

		if (atualizacoes > 0)
			abrangenciasComAtualizacao.push(abrangencia);
	}
	callback();
}

function fazerBackupIndices (callback) {
	logger.info('Backing up index files containing updates')
	async.forEachSeries(abrangenciasComAtualizacao, function (abrangencia, next) {
			fachadaXML.obterZipIndice(abrangencia, function (buffer, path) {
				path = config.raizBackup + '/' + agora + '/' + path;
				logger.info('Backing up index to ' + path)
				filePublisher.publish(path, buffer, function (err) {
					if (err) {
						logger.error('File ' + path + 
							' could not be saved on this iteration due to previous failure',
							err.stack || err);	
					}
					next(err);
				});
			}, function () {
				logger.info('Skipping ' + abrangencia + ' on this iteration due to previous failure');
				next();
			})
	}, callback)

}

function padZero (num) {
	return num < 10 ? '0' + num : num;
}

function fazerBackupAtualizados (callback) {
	var tipos = ['fixos', 'variaveis'];

	async.forEachSeries(tipos, function (tipo, next) {
		async.forEachSeries(atualizacao[tipo], function (nomeZip, next) {
			var uf = nomeZip.substring(0, 2);
			fachadaXML.obterZipDentroAbrangencia(uf, nomeZip, function (buffer, path) {
				path = config.raizBackup + '/' + agora + '/' + path;
				logger.info('Backing up to ' + path)
				filePublisher.publish(path, buffer, function (err) {
					if (err) {
						logger.error('File ' + path + 
							' could not be saved on this iteration due to previous failure',
							err.stack || err);	
					}
					next(err);
				});				

			}, function () {
				logger.error('Skipping file ' + nomeZip + ' on this iteration due to previous failure');
				next();
			})

		}, next)		
	}, function () {
		logger.info('Backup completed')
		callback()
	});	
}

function atualizarFixos (callback) {
	logger.info('Updating static data files (arquivos de dados fixos)')

	async.forEachSeries(atualizacao.fixos, function (nomeZip, next) {
		//var ext = FachadaJsonApuracaoIG.REGEX_MUNICIPIO_FIXO.exec(nomeZip);
		var uf = nomeZip.substr(0, 2);
		fachadaJson.obterFixo(nomeZip, function (jsonFixo) {
			logger.info('Persisting updated version of ' + nomeZip);
			jsonFixo._id = nomeZip;
			persistencia.salvarFixoAbrangencia(jsonFixo, function () {
				indice[uf].fixos[nomeZip] = indiceAndamento[uf].fixos[nomeZip]
				contagemAPersistir[uf]++;
				if (contagemAPersistir[uf] >= MAX_UNSAVED_ENTRIES) {
					logger.info('Unsaved changes on index ' + uf + ' reached max - persisting index');
					persistirIndice(uf, next)
				} else {
					next();
				}

			}, function () {
				logger.error('Skipping file ' + nomeZip + ' on this iteration due to previous failure');
				next();	
			})

		}, function () {
			logger.error('Skipping file ' + nomeZip + ' on this iteration due to previous failure');
			next();
		})

	}, function () {
		async.forEachSeries(config.abrangencias, function (uf, next) {
			if (contagemAPersistir[uf] > 0) {
				logger.info('Saving changes on index ' + uf)
				persistirIndice(uf, next)
			} else {
				next()
			}
		}, callback)
	})
}

function persistirIndice (uf, next) {
	persistencia.salvarIndice(indice[uf], function () {
		contagemAPersistir[uf] = 0;
		next()

	}, function () {
		logger.error('Failed to save changes to index ' + uf);
		next();
	})
}

function atualizarVariaveis (callback) {
	logger.info('Updating variable data files (arquivos de dados variaveis)')
	async.forEachSeries(atualizacao.variaveis, function (nomeZip, next) {
		var metadata = Classificador.classificar(nomeZip);
		var uf = nomeZip.substr(0, 2);
		fachadaJson.obterVariavel(nomeZip, function (jsonVariavel) {
			var nomeDadosFixos = jsonVariavel.resultado.nomeArquivoDadosFixos;
			logger.info('Retrieving locally stored static data file ' + nomeDadosFixos);
			persistencia.obterFixoAbrangencia(nomeDadosFixos, function (dadosFixos) {
				if (!dadosFixos) {
					logger.error('Static data file ' + jsonVariavel.dadosFixos + ' not found - skipping file ' + nomeZip);
					next();	
					return;
				}

				consumingStrategy.consume(metadata, dadosFixos, jsonVariavel, function () {
					indice[uf].variaveis[nomeZip] = indiceAndamento[uf].variaveis[nomeZip]
					contagemAPersistir[uf]++;

					if (contagemAPersistir[uf] >= MAX_UNSAVED_ENTRIES) {
						logger.info('Unsaved changes on index ' + uf + ' reached max - persisting index');
						persistirIndice(uf, next)
					} else {
						next();
					}				

				}, function () {
					logger.error('Skipping file ' + nomeZip + ' on this iteration due to previous failure');
					next();	
				})

			}, function () {
				logger.error('Skipping file ' + nomeZip + ' on this iteration due to previous failure');
				next();	
			})

		}, function () {
			logger.error('Skipping file ' + nomeZip + ' on this iteration due to previous failure');
			next();
		})

	}, function () {
		async.forEachSeries(config.abrangencias, function (uf, next) {
			if (contagemAPersistir[uf] > 0) {
				logger.info('Saving changes on index ' + uf)
				persistirIndice(uf, next)
			} else {
				next()
			}
		}, callback)
	})
}

/*
 * Should never trigger this event. 
 * This is here for safety only.
 * Domains are expected to capture all unhandled exceptions.
 */
process.on('uncaughtException', function (err) {
    logger.error('Uncaught exception - process may have been tainted', err.stack || err)
})

function encerrarComErroFatal (msg) {
	logger.error('Exiting due to fatal error');
	process.exit();
}
