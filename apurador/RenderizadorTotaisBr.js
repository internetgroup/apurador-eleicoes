var utils = require('./utils.js').utils,
    logger = require('winston');

function RenderizadorTotaisBr (regras) {
	this._regras = regras;
}

RenderizadorTotaisBr.prototype.aplicavel = function (dadosFixos) {
	return true;
}

RenderizadorTotaisBr.prototype.renderizar = function (arquivoFixo, arquivoVariavel) {
    this._result = null;

   var total_eleitorado = utils.formatNumero(arquivoFixo.br.eleitorado),
    eleitorado_apurado = utils.formatNumero(arquivoVariavel.stats.eleitoradoApurado),
    perc_eleitorado_apurado = (arquivoVariavel.stats.eleitoradoApurado / arquivoFixo.br.eleitorado * 100).toFixed(0);

    // Eleitorado Apurado
    // Eleitorado
    // Comparecimento
    // Abstenção


    var realtime = { 
        total_eleitorado : total_eleitorado,
        eleitorado_apurado : eleitorado_apurado,
        perc_eleitorado_apurado : perc_eleitorado_apurado,
        comparecimento : arquivoVariavel.stats.comparecimento,
        abstencao : arquivoVariavel.stats.abstencao
    };

    html = '\
        <div class="total-votos-pais">\
        <div class="porcentagem-apuracao nobdl">\
          <big>'+perc_eleitorado_apurado+'% APURADOS</big>\
          <small>'+eleitorado_apurado+' votos</small>\
        </div>\
        <div class="stats votos-validos">\
          <big>Eleitorado</big>\
          <small>'+total_eleitorado+'</small>\
        </div>\
        <div class="stats brancos-e-nulos">\
          <big>Comparecimento</big>\
          <small>'+ utils.formatNumero(arquivoVariavel.stats.comparecimento) + '</small>\
        </div>\
        <div class="stats abstencoes nobdr">\
          <big>Absten&ccedil;&otilde;es</big>\
          <small>'+ utils.formatNumero(arquivoVariavel.stats.abstencao) + '</small>\
        </div>\
        </div>\
    ';

    this._result = {json: JSON.stringify(realtime), html: html};
    return true;
}

RenderizadorTotaisBr.prototype.getDescricao = function () {
	return 'Totais Br'
}

RenderizadorTotaisBr.prototype.getURLPaginaContainer = function (dadosFixos) {
  return [ { url: this._regras.obterURLHomeEleicoes(dadosFixos), seletor: '#total-votos-pais' } ];
}

RenderizadorTotaisBr.prototype.obterCaminhoGravacao = function (dadosFixos, formato) {
    if (formato == 'html')
        return this._regras.obterCaminhoIncludeTotaisBr(dadosFixos);

    if (formato == 'json')
        return this._regras.obterCaminhoJsonTotaisBr(dadosFixos);

    msg = 'No render result for format ' + formato;
    logger.error(msg);
    throw new Error(msg);
}

exports.RenderizadorTotaisBr = RenderizadorTotaisBr;