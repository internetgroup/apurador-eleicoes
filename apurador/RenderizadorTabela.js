var utils = require('./utils.js').utils,
    logger = require('winston');

function RenderizadorTabela (regras) {
	this._regras = regras;
}

RenderizadorTabela.prototype.aplicavel = function (dadosFixos) {
	//return dadosFixos.cargo == '0013';
	return true;
}

RenderizadorTabela.prototype.renderizar = function (arquivoFixo, arquivoVariavel) {
        // console.log('geraTabelaCandidatos');
  this._result = null;

  var candidatos = arquivoFixo.candidatos,
    partidos = arquivoFixo.partidos,
    candidatosVar = arquivoVariavel.candidatos,
        html = '',
        realtime = {}
        realtime["status-cidade"] = []


        candidatos.sort(function(atual,posterior){

          candidato_atual = candidatosVar[atual.numero];
          candidato_posterior = candidatosVar[posterior.numero];

          if (!candidato_atual) return 1;
          if (!candidato_posterior) return -1;

          try {
              var eleito_1 = (candidato_atual.eleito == 'S'),
                  eleito_2 = (candidato_posterior.eleito == 'S'),
                  votos_1 = (candidato_atual.totalVotos),
                  votos_2 = (candidato_posterior.totalVotos);

          } catch (e) {
              console.log(candidato_atual);
              console.log(candidato_posterior);
              throw e;
          }

          if ( (eleito_1 && eleito_2) || (!eleito_1 && !eleito_2))
              return votos_2 - votos_1;
              
          if (eleito_1)
            return -1;

          return 1; 

        })


        if(arquivoFixo.cargo == '0011'){
            //Tabela prefeito
            html += '<table cellpadding="0" cellspacing="0" class="eleicao2012-apuracao-cand" id="cand-prefeito">';
        }else{
            //Tabela vereadores
            html += '<table cellpadding="0" cellspacing="0" class="eleicao2012-apuracao-cand" id="cand-vereador">';
        }

        html +='<thead>\
              <tr>\
                <th class="posicao">Posi&ccedil;&atilde;o</th>\
                <th class="candidato">Candidato</th>\
                <th class="numero">N&uacute;mero</th>\
                <th class="partido">Partido</th>\
                <th class="perc">% Votos</th>\
                <th class="total">Total</th>\
              </tr>\
            </thead>\
            <tfoot></tfoot>\
            <tbody>';

    var countEleitos = 0;
    for (var i=0; i<candidatos.length; i++) {
        if(candidatosVar[candidatos[i].numero] && candidatosVar[candidatos[i].numero].eleito == 'S'){
            countEleitos++;
        }
    }



  for (var i=0; i<candidatos.length; i++) {

      realtime_row = {}
      posicao = i + 1;

      //Verifica que o candidato existe no arquivo de variáveis
      if((candidatosVar[candidatos[i].numero] && parseInt(candidatos[i].situacao) == 2) || (candidatosVar[candidatos[i].numero] && parseInt(candidatos[i].situacao) == 16) || (candidatosVar[candidatos[i].numero] && parseInt(candidatos[i].situacao) == 12) ){
        var porcentagem = ((parseFloat(candidatosVar[candidatos[i].numero].totalVotos) / parseFloat(arquivoVariavel.stats.votosValidos)) * 100).toFixed(2);

        porcentagem = isNaN(porcentagem) ? "0.00" : porcentagem

        //Prefeito 2 turno matematicamente definido
        if(arquivoFixo.cargo == '0011' && arquivoVariavel.matematicamenteDefinido && arquivoVariavel.matematicamenteDefinido == 'S'){
          //Os 2 primeiros vao para 2 turno

          if(i < 2){
            html += '<tr class="in filtrada segundo-turno ">\
                        <td class="posicao">' + posicao +'</td>\
                        <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                        </span></td>\
                        <td class="numero">'+ candidatos[i].numero + '</td>\
                        <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                        <td class="perc">'+ porcentagem + '% - 2º TURNO</td>\
                        <td class="total">' + utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) +'</td>\
                      </tr>';


          }else{
            html += '<tr class="in filtrada">\
                        <td class="posicao">' + posicao +'</td>\
                        <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                        </span></td>\
                        <td class="numero">'+ candidatos[i].numero + '</td>\
                        <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                        <td class="perc">'+ porcentagem + '%</td>\
                        <td class="total">' + utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) +'</td>\
                      </tr>';
          }
          realtime_row = {
              posicao : posicao,
              nome : candidatos[i].nomeUrna,
              numero : candidatos[i].numero,
              partido : partidos[candidatos[i].partido].sigla,
              porcentagem : porcentagem + '%',
              total_votos : candidatosVar[candidatos[i].numero].totalVotos,
              situacao_ok : true
          }

        //Prefeito matematicamente eleito
        }else if(arquivoFixo.cargo == '0011' && arquivoVariavel.matematicamenteDefinido && arquivoVariavel.matematicamenteDefinido == 'E'){
          if(i < 1){
            html += '<tr class="in filtrada cand-eleito ">\
                        <td class="posicao">' + posicao +'</td>\
                        <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                        </span></td>\
                        <td class="numero">'+ candidatos[i].numero + '</td>\
                        <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                        <td class="perc">'+ porcentagem + '% - ELEITO</td>\
                        <td class="total">' + utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) +'</td>\
                      </tr>';


          }else{
            html += '<tr class="in filtrada">\
                        <td class="posicao">' + posicao +'</td>\
                        <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                        </span></td>\
                        <td class="numero">'+ candidatos[i].numero + '</td>\
                        <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                        <td class="perc">'+ porcentagem + '%</td>\
                        <td class="total">' + utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) +'</td>\
                      </tr>';
          }
          realtime_row = {
              posicao : posicao,
              nome : candidatos[i].nomeUrna,
              numero : candidatos[i].numero,
              partido : partidos[candidatos[i].partido].sigla,
              porcentagem : porcentagem + '%',
              total_votos : candidatosVar[candidatos[i].numero].totalVotos,
              situacao_ok : true
          }
        }else{
          //Eleição para prefeito, candidato eleito e mais do que um eleito = 2 turno
          if(arquivoFixo.cargo == '0011' && countEleitos > 1 && candidatosVar[candidatos[i].numero].eleito == 'S'){
            html += '<tr class="in filtrada segundo-turno">\
                        <td class="posicao">' + posicao +'</td>\
                        <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                        </span></td>\
                        <td class="numero">'+ candidatos[i].numero + '</td>\
                        <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                        <td class="perc">'+ porcentagem + '% - 2º TURNO</td>\
                        <td class="total">' + utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) +'</td>\
                      </tr>';
          // Candidato eleito com menos do que 1 eleito ou se for vereador
          }else if(candidatosVar[candidatos[i].numero].eleito == 'S'){
            html += '<tr class="in filtrada cand-eleito">\
                        <td class="posicao">' + posicao +'</td>\
                        <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                        </span></td>\
                        <td class="numero">'+ candidatos[i].numero + '</td>\
                        <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                        <td class="perc">'+ porcentagem + '% - ELEITO</td>\
                        <td class="total">' + utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) +'</td>\
                      </tr>';
          }else{
            html += '<tr class="in filtrada">\
                        <td class="posicao">' + posicao +'</td>\
                        <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                        </span></td>\
                        <td class="numero">'+ candidatos[i].numero + '</td>\
                        <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                        <td class="perc">'+ porcentagem + '%</td>\
                        <td class="total">' + utils.formatNumero(candidatosVar[candidatos[i].numero].totalVotos) +'</td>\
                      </tr>';
          }
          realtime_row = {
                posicao : posicao,
                nome : candidatos[i].nomeUrna,
                numero : candidatos[i].numero,
                partido : partidos[candidatos[i].partido].sigla,
                porcentagem : porcentagem + '%',
                total_votos : candidatosVar[candidatos[i].numero].totalVotos,
                situacao_ok : true
            }
        }
      //Candidato não está nos arquivos variáveis
      }else{
            //Verifica a situação do candidato
            switch(parseInt(candidatos[i].situacao))
            {
            case 1:
              situacao = 'Cadastrado';
              break;
            case 2:
              situacao = 'Deferido';
              break;
            case 3:
              situacao = 'Inapto';
              break;
            case 4:
              situacao = 'Indeferido com recurso';
              break;
            case 5:
              situacao = 'Cancelado';
              break;
            case 6:
              situacao = 'Ren&uacute;ncia';
              break;
            case 7:
              situacao = 'Falecido';
              break;
            case 8:
              situacao = 'Aguardando julgamento';
              break;
            case 10:
              situacao = 'Cassado';
              break;
            case 11:
              situacao = 'Impugnado';
              break;
            case 12:
              situacao = 'Apto';
              break;
            case 13:
              situacao = 'N&atilde;o conhecimento do pedido';
              break;
            case 14:
              situacao = 'Indeferido';
              break;
            case 15:
              situacao = 'Com not&iacute;cia de inelegibilidade';
              break;
            case 16:
              situacao = 'Deferido com recurso';
              break;
            case 17:
              situacao = 'Substituto majorit&aacute;rio pendente de julgamento';
              break;
            case 18:
              situacao = 'Cassado com recurso';
              break;
            default:
              situacao = 'Outros';
            }

            if (parseInt(candidatos[i].situacao) == 2 || parseInt(candidatos[i].situacao) == 16) {
                html += '<tr class="in filtrada">\
                            <td class="posicao"> - </td>\
                            <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                            </span></td>\
                            <td class="numero">'+ candidatos[i].numero + '</td>\
                            <td class="partido">'+ partidos[candidatos[i].partido].sigla + '</td>\
                            <td class="perc"> 0,00% </td>\
                            <td class="total">0</td>\
                          </tr>';
                
                realtime_row = {
                    posicao : "-",
                    nome : candidatos[i].nomeUrna,
                    numero : candidatos[i].numero,
                    partido : partidos[candidatos[i].partido].sigla,
                    porcentagem : '0,00%',
                    total_votos : 0,
                    situacao_ok : true
                }

            }else{
                html += '<tr class="">\
                            <td class="posicao"> - </td>\
                            <td class="candidato"><span>'+ candidatos[i].nomeUrna + '\
                            </span></td>\
                            <td class="numero">'+ candidatos[i].numero + '</td>\
                            <td class="situacao" colspan="3">'+ situacao + '</td>\
                          </tr>';
                realtime_row = {
                    posicao : "-",
                    nome : candidatos[i].nomeUrna,
                    numero : candidatos[i].numero,
                    partido : partidos[candidatos[i].partido].sigla,
                    porcentagem : '0,00%',
                    total_votos : 0,
                    situacao_ok : false,
                    motivo : situacao
                }
            }


      }


        realtime["status-cidade"].push(realtime_row)

  }
  html += '</tbody></table>';
  //console.log(html);

    // fs.writeFile('tabela.html',html);

  this._result = {json: JSON.stringify(realtime), html: html};
  return true;
}

RenderizadorTabela.prototype.getDescricao = function () {
	return 'Tabela'
}

RenderizadorTabela.prototype.getURLPaginaContainer = function (dadosFixos) {
  var paginas = [ { url: this._regras.obterURLPaginaMunicipio(dadosFixos), 
             seletor: '#cand-' + (dadosFixos.cargo == '0013' ? 'vereador' : 'prefeito') } ];

  // TODO: Por esse ID de sao-paulo nos properties
  if (dadosFixos.cargo == '0013' && dadosFixos.municipio.codigo == '71072') {
    // Gambiarra feita na vespera da eleicao
    var pag = { url: this._regras.obterURLHomeEleicoes(dadosFixos), 
                seletor: '#cand-vereador',
                vereador_home: true };
    paginas.push(pag)
  }

  return paginas;
}

RenderizadorTabela.prototype.obterCaminhoGravacao = function (dadosFixos, formato) {
    if (dadosFixos.cargo == '0013') {
        if (formato == 'html')
            return this._regras.obterCaminhoIncludeVereador(dadosFixos)
        if (formato == 'json')
            return this._regras.obterCaminhoJsonVereador(dadosFixos)
    }

    if (dadosFixos.cargo == '0011') {
        if (formato == 'html')
            return this._regras.obterCaminhoIncludePrefeito(dadosFixos)
        if (formato == 'json')
            return this._regras.obterCaminhoJsonPrefeito(dadosFixos)
    }

    msg = 'No render result for format ' + formato + ' and cargo field ' + dadosFixos.cargo;
    logger.error(msg);
    throw new Error(msg);
}

exports.RenderizadorTabela = RenderizadorTabela;