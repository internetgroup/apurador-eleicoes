var utils = exports.utils = {
    parseInt: function (strValue) {
        var value = parseInt(strValue);
        if (isNaN(value))
            value = 0;
        return value;
    },

    calculatePercentage: function (parcial, total) {
        return total == 0 ? 0 : (parcial * 100 / total);        
    },

    createVersion: function () {
        var now = new Date();
        var version = '' + utils.pad(now.getFullYear(), '0000') + 
                '/' + utils.pad(now.getMonth() + 1, '00') + 
                '/' + utils.pad(now.getDate(), '00') +
                '/' + utils.pad(now.getHours(), '00') + 
                '/' + utils.pad(now.getMinutes(), '00') + 
                '/' + now.getTime();        
        return version;
    },

    formatBr1Decimal: function (number) {
        var number = parseFloat(number) || 0;

        number = parseInt(number * 10) / 10;   // Avoiding 0.0007 => 0.0

        return (number + '').replace(/\.(\d)\d*$/, ',$1');
    },

    pad: function (num, zeros) {
        zeros += '';
        return (zeros + num).slice(-zeros.length);
    },

    toTitleCase: function (e) {
        e = e.toLowerCase();
        var t=/^(a|e|as|de|da|del|dos|do|das|des|la|vs?\.?|via)$/i;
        return e.replace(/([^\W_]+[^\s-]*) */g,function(e,n,r,i){
            return r>0&&r+n.length!==i.length&&n.search(t)>-1&&i.charAt(r-2)!==":"&&i.charAt(r-1).search(/[^\s-]/)<0?e.toLowerCase():n.substr(1).search(/[A-Z]|\../)>-1?e:e.charAt(0).toUpperCase()+e.substr(1);
        })
    },

	formatNumero: function (nStr) {
	    nStr += '';
	    x = nStr.split('.');
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + '.' + '$2');
	    }
	    return x1 + x2;
	},

	normalizarNome: function (nome) {
        var resultado = nome;
        var caracteresAcento = {
            "Á" : "A", "á" : "a",  "É" : "E", "é" : "e",  "Í" : "I", "í" : "i",
            "Ó" : "O", "ó" : "o",  "Ú" : "U", "ú" : "u",  "À" : "A", "à" : "a",
            "È" : "E", "è" : "e",  "Ì" : "I", "ì" : "i",  "Ò" : "O", "ò" : "o",
            "Ù" : "U", "ù" : "u",  "Â" : "A", "â" : "a",  "Ê" : "E", "ê" : "e",
            "Î" : "I", "î" : "i",  "Ô" : "O", "ô" : "o",  "Û" : "U", "û" : "u",
            "Ä" : "A", "ä" : "a",  "Ë" : "E", "ë" : "e",  "Ï" : "I", "ï" : "i",
            "Ö" : "O", "ö" : "o",  "Ü" : "U", "ü" : "u",  "Ã" : "A", "ã" : "a",
            "Õ" : "O", "õ" : "o",  "Ç" : "C", "ç" : "c"
	    };

        for (caracter in caracteresAcento) {
                resultado = resultado.replace(new RegExp(caracter, 'gi'), caracteresAcento[caracter]);
        }

        // Removendo espaços no início/fim e substituíndo espaços no meio por "+".  
        resultado = resultado.replace(/^\s+/g, "");
        resultado = resultado.replace(/\s+$/g, "");
        resultado = resultado.replace(/\s+/g, "-");

        resultado = resultado.toLowerCase();

        // Retirando os caracateres restantes:
        resultado = resultado.replace(/[^-a-z0-9+]/g, '');
        return resultado; // TODO Colocar o nome correto.
	}
}