var logger = require('winston');
var util = require('util');
var BaseLogic = require('./base');

function TemplateLogic () {}

TemplateLogic.prototype.preProcessData = function (data) {
	logger.info('Processing data');

	var data = BaseLogic.prototype.preProcessData.apply(this, [data]);

	var votos = data.dadosVariaveis.abrangencias[0].votosCandidato;
	
	data.dadosVariaveis.abrangencias[0].votosCandidato = votos.slice(0, 2);

	data.dadosFixos.dadosFixos.cargo = 'Top Presidente';

	return data;
}

module.exports = TemplateLogic;
