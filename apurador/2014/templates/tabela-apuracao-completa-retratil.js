var logger = require('winston');
var util = require('util');
var BaseLogic = require('./base');

function TemplateLogic () {}

TemplateLogic.prototype.preProcessData = function (data) {
	logger.info('Processing data');

	var data = BaseLogic.prototype.preProcessData.apply(this, [data]);

	return data;
}

module.exports = TemplateLogic;