var logger = require('winston');
var util = require('util');
var BaseLogic = require('./base');
var appUtils = require('../../utils').utils;

function TemplateLogic () {}

TemplateLogic.prototype.preProcessData = function (data) {
	logger.info('Processing data');

	// console.log(util.inspect(data.dadosVariaveis, {depth: null}))

	var data = BaseLogic.prototype.preProcessData.apply(this, [data]);

	var abrangencias = data.dadosVariaveis.abrangencias;

	for (var i = 0; i < abrangencias.length; i++) {
		var abrangencia = abrangencias[i];
		if (abrangencia.tipoAbrangencia == 'UF') {
			var br = abrangencia;
			var eleitoradoApurado = appUtils.parseInt(br.eleitoradoApurado);
			var eleitoradoNaoApurado = appUtils.parseInt(br.eleitoradoNaoApurado);

			br.porcentagemEleitoradoApurado = appUtils.calculatePercentage(
				eleitoradoApurado,
				eleitoradoApurado + eleitoradoNaoApurado);

			return { br: br };
		}
	}

	return {};
}

module.exports = TemplateLogic;