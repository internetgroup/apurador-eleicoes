var logger = require('winston');
var util = require('util');
var BaseLogic = require('./base');

function TemplateLogic () {}

TemplateLogic.prototype.preProcessData = function (data) {
	logger.info('Processing data');

	// console.log(util.inspect(data, {depth: null}))

	var data = BaseLogic.prototype.preProcessData.apply(this, [data]);

	var votos = data.dadosVariaveis.abrangencias[0].votosCandidato;
	
	data.dadosVariaveis.abrangencias[0].votosCandidato = votos.slice(0, 3);

	data.dadosFixos.dadosFixos.cargo = 'Deputado federal';

	return data;
}

module.exports = TemplateLogic;