var Classificador = require('../../ClassificadorArquivos');
var utils = require('../../utils').utils;
var urls = require('../urls');
var util = require('util');

function BaseLogic () {}

BaseLogic.prototype.preProcessData = function (data) {
	var candidatos = data.dadosFixos.candidatos;
	var partidos = data.dadosFixos.partidos;
	var votosCandidato = data.dadosVariaveis.abrangencias[0].votosCandidato;

	// Capitalizando nomes de candidato
	for (var i in candidatos) {
		candidatos[i].nome = utils.toTitleCase(candidatos[i].nome + '');
		candidatos[i].nomeUrna = utils.toTitleCase(candidatos[i].nomeUrna + '');
	}

	// Capitalizando nome do estado
	data.dadosFixos.dadosFixos.nomeAbrangencia = utils.toTitleCase(data.dadosFixos.dadosFixos.nomeAbrangencia);

	// Ordenando candidatos
	votosCandidato.sort(function (voto1, voto2) {
		var candidato1 = candidatos[voto1.numeroCandidato];
		var candidato2 = candidatos[voto2.numeroCandidato];

		if (!candidato1) return 1;
		if (!candidato2) return -1;

		var eleito1 = (voto1.eleito === 'S');
		var eleito2 = (voto2.eleito === 'S');
		var total1 = voto1.totalVotos;
		var total2 = voto2.totalVotos;

		if (eleito1 && eleito2) {
			return total2 - total1;
		}

		if (eleito1) {
			return -1;
		}

		if (eleito2) {
			return 1;
		}

		if (total1 == total2) {
			return (candidato1.nomeUrna < candidato2.nomeUrna ? -1 : 1);
		}

		return total2 - total1;
	});

	var votosValidos = parseInt(data.dadosVariaveis.abrangencias[0].votosValidos);
	if (isNaN(votosValidos))
		votosValidos = 0;

	var eleitos = [];

	// Calculando porcentagem de votos de cada candidato
	for (var i = 0; i < votosCandidato.length; i++) {
		var voto = votosCandidato[i];
		var candidato = candidatos[voto.numeroCandidato];
		voto.dadosFixosCandidato = candidato;

		if (candidato) {
			candidato.dadosFixosPartido = partidos[candidato.numeroPartido];
		}

		if (votosValidos == 0) {
			voto.porcentagemValidos = 0;
		} else {
			var totalVotosCandidato = parseInt(voto.totalVotos);
			if (isNaN(totalVotosCandidato))
				totalVotosCandidato = 0;
			voto.porcentagemValidos = (totalVotosCandidato * 100 / votosValidos); 
		}

		voto.posicao = i + 1;

		if (voto.eleito === 'S')
			eleitos.push(i);
		else
			voto.className = 'current';
	}

	// Calculando situacao dos candidatos
	var cargoTemSegundoTurno = (
			data.metadata.cargo == Classificador.CARGO_PRESIDENTE ||
			data.metadata.cargo == Classificador.CARGO_GOVERNADOR );

	if (cargoTemSegundoTurno && eleitos.length == 2) {
		for (var i = 0; i < eleitos.length; i++) {
			votosCandidato[eleitos[i]].className = 'second-round';
		}
	} else {
		for (var i = 0; i < eleitos.length; i++) {
			votosCandidato[eleitos[i]].className = 'winner';
		}
	}

	// Bug do TSE! Nem sempre ele traz os eleitos marcados
	if ( (data.dadosVariaveis.resultado.matematicamenteDefinido == 'S' ||
			data.dadosVariaveis.resultado.matematicamenteDefinido == 'E' ) 
			&& eleitos.length == 0
			&& cargoTemSegundoTurno) {

		var temEleito = false;
		for (var i = 0; i < votosCandidato.length; i++) {
			var voto = votosCandidato[i];

			if (voto.porcentagemValidos > 50) {
				temEleito = true;
				voto.eleito = 'S';
				voto.className = 'winner';
				break;
			}
		}

		if (!temEleito) {
			for (var i = 0; i < votosCandidato.length && i < 2; i++) {
				var voto = votosCandidato[i];
				voto.eleito = 'S';
				voto.className = 'second-round';
			}
		}

	// Bug do TSE! Nem sempre ele traz os eleitos marcados
	} else if ( (data.dadosVariaveis.resultado.matematicamenteDefinido == 'S' ||
			data.dadosVariaveis.resultado.matematicamenteDefinido == 'E' ) 
			&& eleitos.length == 0
			&& data.metadata.cargo == Classificador.CARGO_SENADOR) {

		for (var i = 0; i < votosCandidato.length && i < 1; i++) {
			var voto = votosCandidato[i];
			voto.eleito = 'S';
			voto.className = 'winner';
		}
	}	

	// Calculando porcentagem do eleitorado apurado
	var eleitoradoNaoApurado = parseInt(data.dadosVariaveis.abrangencias[0].eleitoradoNaoApurado);
	if (isNaN(eleitoradoNaoApurado))
		eleitoradoNaoApurado = 0;

	var eleitoradoApurado = parseInt(data.dadosVariaveis.abrangencias[0].eleitoradoApurado);
	if (isNaN(eleitoradoApurado))
		eleitoradoApurado = 0;

	if (data.dadosVariaveis.abrangencias[0].tipoAbrangencia == 'BR') {
		// Workaround durante a apuracao, porque o TSE estava mandando dados incorretos
		var totalEleitorado = 142822046;
	} else {
		var totalEleitorado = eleitoradoNaoApurado + eleitoradoApurado;	
	}

	data.dadosVariaveis.abrangencias[0].porcentagemEleitoradoApurado = 
		(totalEleitorado == 0 ? 0 : eleitoradoApurado * 100 / totalEleitorado);

	data.metadata.nomeCargoImagem = urls.nomesCargoImagem[data.metadata.cargo];
	data.dadosFixos.dadosFixos.nomeEstadoUrl = urls.nomesUfUrl[data.metadata.idAbrangencia];

    return data;	
}

module.exports = BaseLogic;