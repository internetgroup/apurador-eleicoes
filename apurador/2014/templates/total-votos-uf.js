var logger = require('winston');
var util = require('util');
var appUtils = require('../../utils').utils;
var BaseLogic = require('./base');

function TemplateLogic () {}

TemplateLogic.prototype.preProcessData = function (data) {
	logger.info('Processing data');

	// console.log(util.inspect(data, {depth: null}))

	var data = BaseLogic.prototype.preProcessData.apply(this, [data]);

	var votos = data.dadosVariaveis.abrangencias[0].votosCandidato;
	
	// data.dadosVariaveis.abrangencias[0].votosCandidato = votos.slice(0, 3);

	var abrangencias = data.dadosVariaveis.abrangencias;

	var ufs = {}

	for (var i = 0; i < abrangencias.length; i++) {
		if (abrangencias[i].tipoAbrangencia == 'UF' &&
			abrangencias[i].codigoAbrangencia != 'ZZ') {
			var abrangencia = abrangencias[i];
			var nomeUf = abrangencia.codigoAbrangencia;
			if (!ufs[nomeUf]) {
				ufs[nomeUf] = abrangencia;
				continue;
			}
				
			var newDate = parseDate(abrangencia.dataTotalizacao, abrangencia.horaTotalizacao);
			var storedDate = parseDate(ufs[nomeUf].dataTotalizacao, ufs[nomeUf].horaTotalizacao);

			if (!newDate || !storedDate)
				continue;

			if (newDate.getTime() > storedDate.getTime())
				ufs[nomeUf] = abrangencia;
		}
	}

	for (var ufName in ufs) {
		var uf = ufs[ufName];

		var eleitoradoApurado = appUtils.parseInt(uf.eleitoradoApurado);
		var eleitoradoNaoApurado = appUtils.parseInt(uf.eleitoradoNaoApurado);

		uf.porcentagemEleitoradoApurado = appUtils.calculatePercentage(
			eleitoradoApurado,
			eleitoradoApurado + eleitoradoNaoApurado);
	}

	return { dadosFixos: data.dadosFixos, abrangencias: ufs };
}

function parseDate (strDate, strTime) {
	var extDate = /^(\d+)\/(\d+)\/(\d+)$/.exec(strDate);
	if (!extDate)
		return null;
	var day = parseInt(extDate[1]);
	var month = parseInt(extDate[2]) - 1; 
	var year = parseInt(extDate[3]);

	var extTime = /^(\d+):(\d+):(\d+)$/.exec(strTime);
	if (!extTime)
		return null;
	var hour = parseInt(extTime[1]);
	var minute = parseInt(extTime[2]); 
	var second = parseInt(extTime[3]);

	return new Date(year, month, day, hour, minute, second);
}

module.exports = TemplateLogic;