var logger = require('winston');
var util = require('util');
var BaseLogic = require('./base');
var appUtils = require('../../utils').utils;

function TemplateLogic () {}

TemplateLogic.prototype.preProcessData = function (data) {
	logger.info('Processing data');

	var data = BaseLogic.prototype.preProcessData.apply(this, [data]);

	var votos = data.dadosVariaveis.abrangencias[0].votosCandidato;

	var top3 = votos.slice(0, 3);

	var json = { 
		resultado: [], 
		porcentagemEleitoradoApurado: appUtils.formatBr1Decimal(data.dadosVariaveis.abrangencias[0].porcentagemEleitoradoApurado) 
	};

	for (var i = 0; i < top3.length; i++) {
		var candidato = top3[i];

		var situacao = 'normal';
		if (candidato.className == 'winner') {
			situacao = 'eleito';
		} else if (candidato.className == 'second-round') {
			situacao = 'segundo-turno';
		}

		json.resultado.push({
			nome: candidato.dadosFixosCandidato.nomeUrna,
			numero: candidato.dadosFixosCandidato.numero,
			partido: candidato.dadosFixosCandidato.dadosFixosPartido.sigla,
			porcentagem: appUtils.formatBr1Decimal(candidato.porcentagemValidos),
			situacao: situacao
		})
	}

	return json;
}

module.exports = TemplateLogic;