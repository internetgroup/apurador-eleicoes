var Classificador = require('../../ClassificadorArquivos');
var IncludeCMS = require('../../consumidores/IncludeCMS');
var PaginatedCMSInclude = require('../../consumidores/PaginatedCMSInclude');
var ResultSplitter = require('../ResultSplitter');
// TODO: change this to a better solution
var config = require('../../' + process.argv[2] + '/apurador-cfg.js').config;
var IncludePaginator = require('../../consumidores/IncludePaginator');
var util = require('util');
var urls = require('../urls');
var DustTemplate = require('../../template-formats/DustTemplate');
var JsonTemplate = require('../../template-formats/JsonTemplate');

var canalHome = urls.canalHome;
var canaisCargo = urls.canaisCargo;
var mapaCanaisEstado = urls.mapaCanaisEstado;
var mapaCanaisCargo = urls.mapaCanaisCargo;

var resultSplitter = new ResultSplitter(10);

function CMSIncludeFactory () {}

CMSIncludeFactory.prototype.makeIncludesForData = function (metadata, dadosFixos, dadosVariaveis) {
	var rootDir = config.publishDestination;

	var includeData = {
			metadata: metadata,
			dadosFixos: dadosFixos,
			dadosVariaveis: dadosVariaveis,
			toString: function () {return metadata.nomeArquivo}
		};

	if (metadata.tipoAbrangencia == 'br' && 
			metadata.cargo == Classificador.CARGO_PRESIDENTE) {
                
                //Include ultilizado na Home e na página de Cargo
                var includeTopPresidente = new IncludeCMS(
                  'top-2-candidatos-foto.js',
                  new DustTemplate('top-2-candidatos-foto.html'),
                  '2turno-top-presidente-cargo',
                  [canalHome].concat(canaisCargo),
                  includeData);

                //Ultilizado na Home
		var includeTotalVotos = new IncludeCMS(
		  'total-votos-apurados.js',
		  new DustTemplate('total-votos-apurados.html'),
		  '2turno-total-votos-apurados-br',
		  [canalHome],
		  includeData);

		var jsonHome = new IncludeCMS(
			'json-top-3.js',
			new JsonTemplate('handleResponseApuracaoPresidente'),
			'eleicao-home-apuracao-presidente',
			['eleicao-home-apuracao-presidente'],
			includeData);		

		return [includeTopPresidente, includeTotalVotos, jsonHome ];
	}

	if (metadata.tipoAbrangencia == 'br' && 
			metadata.isAcompanhamento) {

		var includeTotalUf = new IncludeCMS(
			'total-votos-uf.js',
			new DustTemplate('total-votos-uf.html'),
			'2turno-total-votos-uf-home',
			[canalHome],
			includeData);	

		return [ includeTotalUf ];
	}	

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.isAcompanhamento) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeUf = new IncludeCMS(
			'total-votos-apurados-uf.js',
			new DustTemplate('total-votos-apurados.html'),
			'2turno-total-votos-apurados-' + metadata.idAbrangencia,
			[canalUf],
			includeData);	

		return [ includeUf ];
	}	

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_PRESIDENTE) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

                //Include ultilizado na Home e na página d Estado
                var includeTopCandidato = new IncludeCMS(
                  '2turno-top-candidato-foto.js',
                  new DustTemplate('2turno-top-candidato-foto.html'),
                  '2turno-top-presidente-' + metadata.idAbrangencia,
                  [canalUf],
                  includeData);

                //Utilizado na página d Cargo
		var includeTopSimples = new IncludeCMS(
		  '2turno-top-candidatos-simples.js',
		  new DustTemplate('top-candidatos-simples.html'),
		  '2turno-top-simples-presidente-' + metadata.idAbrangencia,
		  [mapaCanaisCargo.presidente],
		  includeData);

		return [ includeTopCandidato, includeTopSimples];
	}	

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_GOVERNADOR) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

                //Include ultilizado na página de Estado
                var includeTopCandidato = new IncludeCMS(
                  '2turno-top-candidato-foto.js',
                  new DustTemplate('2turno-top-candidato-foto.html'),
                  '2turno-top-governador-' + metadata.idAbrangencia,
                  [canalUf],
                  includeData);

		var includeTopSimples = new IncludeCMS(
			'2turno-top-candidatos-simples.js',
			new DustTemplate('top-candidatos-simples.html'),
			'2turno-top-simples-governador-' + metadata.idAbrangencia,
			[canalHome, mapaCanaisCargo.governador],
			includeData);

		var includes = [ includeTopSimples, includeTopCandidato ];

		var uf = metadata.idAbrangencia;

		if (uf == 'df' || uf == 'rs' || uf == 'rj') {
			var jsonHome = new IncludeCMS(
				'json-top-3.js',
				new JsonTemplate('handleResponseApuracaoGovernador' + uf.toUpperCase()),
				'eleicao-home-apuracao-governador-' + metadata.idAbrangencia,
				['eleicao-home-apuracao-governador-' + metadata.idAbrangencia],
				includeData);

			includes.push(jsonHome);			
		}

		return includes;
	}	

	return [];
}

module.exports = CMSIncludeFactory;
