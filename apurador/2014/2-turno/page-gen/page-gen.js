var urls = require('../../urls');
var config = require('../../../' + process.argv[2] + '/apurador-cfg.js').config;
var path = require('path');
var Renderer = require('../../../render-engines/DustRenderer');
var FilePublisher = require('../../../FilePublisher');
var publishPageContainer = require('../../publishPageContainer');
var async = require('async');
var logger = winston = require('winston');

var urlHome = urls.urlHome;
var mapaUrlsEstado = urls.mapaUrlsEstado;
var mapaUrlsCargo = urls.mapaUrlsCargo;
var nomesUfUrl = urls.nomesUfUrl;
var nomesUf = urls.nomesUf;
var nomesCargo = urls.nomesCargo;
var nomesCargoCapitalizado = urls.nomesCargoCapitalizado;

logger.remove(winston.transports.Console);
logger.add(winston.transports.Console, {timestamp: true, colorize: true});

var basePath = config.staticPageDestination;

var pages = [
	// home de apuracao
	{
		title: 'Eleições 2014 - Apuração 2º turno - iG',
		description: 'Eleições 2014: acompanhe no iG a apuração do 2º turno das eleições 2014 em tempo real',
		template: '2turno-apuracao-home',
		path: path.join(basePath, 'index.html')
	}
]

for (var uf in mapaUrlsEstado) {
	pages.push({
		title: 'Eleições 2014 - Apuração 2º turno ' + nomesUf[uf] + ' - iG',
		description: 'Eleições 2014: acompanhe no iG a apuração do 2º turno das eleições 2014 em tempo real - ' +
				nomesUf[uf],
		template: '2turno-apuracao-estado',
		path: path.join(basePath, nomesUfUrl[uf], 'index.html'),
		nomeEstado: nomesUf[uf],
		estado: uf
	})
}

pages.push({
	title: 'Eleições 2014 - Apuração 2º turno para ' + nomesCargo.governador + ' - iG',
	description: 'Eleições 2014: acompanhe no iG a apuração do ' +
			'2º turno das eleições 2014 em tempo real para ' +
			nomesCargo.governador,
	template: '2turno-apuracao-' + nomesCargo.governador,
	path: path.join(basePath, 'governador', 'index.html'),
	cargo: 'governador',
	nomeCargo: nomesCargoCapitalizado.governador
})

pages.push({
	title: 'Eleições 2014 - Apuração 2º turno para ' + nomesCargo.presidente + ' - iG',
	description: 'Eleições 2014: acompanhe no iG a apuração do ' +
			'2º turno das eleições 2014 em tempo real para ' +
			nomesCargo.presidente,
	template: '2turno-apuracao-' + nomesCargo.presidente,
	path: path.join(basePath, 'presidente', 'index.html'),
	cargo: 'presidente',
	nomeCargo: nomesCargoCapitalizado.presidente
})

var renderer = new Renderer(path.join(__dirname, '../page-templates'));

renderer.ensureLoadedTemplate('base-page.html');
renderer.ensureLoadedTemplate('2turno-apuracao-home.html');
renderer.ensureLoadedTemplate('2turno-apuracao-estado.html');
renderer.ensureLoadedTemplate('2turno-apuracao-governador.html');
renderer.ensureLoadedTemplate('2turno-apuracao-presidente.html');

var publisher = new FilePublisher()

async.eachSeries(pages,
		publishPage,
		function () {
			logger.info('Finished publishing pages');
		});

function publishPage (page, cb) {
	publishPageContainer(page, renderer, publisher, cb);
}