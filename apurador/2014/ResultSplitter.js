var logger = require('winston');

function ResultSplitter (pageLength) {
	this._pageLength = pageLength;
}

ResultSplitter.prototype.split = function (resultData) {
	var pageLength = this._pageLength;
	var pages = [];

	var pageData = resultData;

	if (pageData &&
			pageData.dadosVariaveis &&
			pageData.dadosVariaveis.abrangencias &&
			pageData.dadosVariaveis.abrangencias[0]) {
	
		var votosCandidato = resultData.dadosVariaveis.abrangencias[0].votosCandidato;
		var numPages = Math.ceil(votosCandidato.length / pageLength);

		logger.info('Splitting result with size', votosCandidato.length, 
				'into', numPages, 'pages with size', pageLength);

		for (var i = 0; i < numPages; i++) {
			var abrangencia = shallowCopy(pageData.dadosVariaveis.abrangencias[0]);
			var abrangencias = [ abrangencia ];
			var dadosVariaveis = shallowCopy(dadosVariaveis);
			var pageData = shallowCopy(pageData);

			pageData.dadosVariaveis = dadosVariaveis;
			dadosVariaveis.abrangencias = abrangencias;

			pageData.pagination = {
				totalResults: votosCandidato.length,
				currentPage: i + 1,
				totalPages: numPages 
			}

			pageData.dadosVariaveis.abrangencias[0].votosCandidato = 
				votosCandidato.slice(i * pageLength, (i + 1) * pageLength);
			pages.push(pageData);
		}

	} else {
		logger.warn('Could not split result: unknown result data format');
	}

	return pages;
}

function shallowCopy (obj) {
	var copy = {};
	for (var i in obj) {
		copy[i] = obj[i];
	}
	return copy;
}

module.exports = ResultSplitter;