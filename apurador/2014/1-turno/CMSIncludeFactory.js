var Classificador = require('../../ClassificadorArquivos');
var IncludeCMS = require('../../consumidores/IncludeCMS');
var PaginatedCMSInclude = require('../../consumidores/PaginatedCMSInclude');
var ResultSplitter = require('../ResultSplitter');
// TODO: change this to a better solution
var config = require('../../' + process.argv[2] + '/apurador-cfg.js').config;
var IncludePaginator = require('../../consumidores/IncludePaginator');
var util = require('util');
var urls = require('../urls');
var DustTemplate = require('../../template-formats/DustTemplate');
var JsonTemplate = require('../../template-formats/JsonTemplate');

var canalHome = urls.canalHome;
var canaisCargo = urls.canaisCargo;
var mapaCanaisEstado = urls.mapaCanaisEstado;
var mapaCanaisCargo = urls.mapaCanaisCargo;

var resultSplitter = new ResultSplitter(10);

function CMSIncludeFactory () {}

CMSIncludeFactory.prototype.makeIncludesForData = function (metadata, dadosFixos, dadosVariaveis) {
	var rootDir = config.publishDestination;

	var includeData = {
			metadata: metadata,
			dadosFixos: dadosFixos,
			dadosVariaveis: dadosVariaveis,
			toString: function () {return metadata.nomeArquivo}
		};

	if (metadata.tipoAbrangencia == 'br' && 
			metadata.cargo == Classificador.CARGO_PRESIDENTE) {

		var includeHome = new IncludeCMS(
			'top-3-candidatos-foto.js',
			new DustTemplate('top-3-candidatos-foto.html'),
			'1turno-top-presidente-home',
			[canalHome],
			includeData);

		var jsonHome = new IncludeCMS(
			'json-top-3.js',
			new JsonTemplate('handleResponseApuracaoPresidente'),
			'eleicao-home-apuracao-presidente',
			['eleicao-home-apuracao-presidente'],
			includeData);

		var includeCargo = new IncludeCMS(
			'top-4-candidatos-foto.js',
			new DustTemplate('top-4-candidatos-foto.html'),
			'1turno-top-presidente-cargo',
			[mapaCanaisCargo.presidente],
			includeData);

		var includeTabelaRetratil = new PaginatedCMSInclude(
			'tabela-apuracao-completa-retratil.js',
			new DustTemplate('tabela-apuracao-completa-retratil.html'),
			'1turno-tabela-apuracao-completa-presidente-cargo',
			[mapaCanaisCargo.presidente],
			includeData,
			resultSplitter);

		var includeTotalVotos = new IncludeCMS(
			'total-votos-apurados.js',
			new DustTemplate('total-votos-apurados.html'),
			'1turno-total-votos-apurados-br',
			[canalHome].concat(canaisCargo),
			includeData);

		return [ includeHome, jsonHome, includeCargo, includeTabelaRetratil, includeTotalVotos ];
	}

	if (metadata.tipoAbrangencia == 'br' && 
			metadata.isAcompanhamento) {

		var includeTotalUf = new IncludeCMS(
			'total-votos-uf.js',
			new DustTemplate('total-votos-uf.html'),
			'1turno-total-votos-uf-home',
			[canalHome],
			includeData);	

		return [ includeTotalUf ];
	}	

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.isAcompanhamento) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeUf = new IncludeCMS(
			'total-votos-apurados-uf.js',
			new DustTemplate('total-votos-apurados.html'),
			'1turno-total-votos-apurados-' + metadata.idAbrangencia,
			[canalUf],
			includeData);	

		return [ includeUf ];
	}	

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_PRESIDENTE) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeTopCandidato = new IncludeCMS(
			'top-4-candidatos-foto.js',
			new DustTemplate('top-4-candidatos-foto.html'),
			'1turno-top-presidente-' + metadata.idAbrangencia,
			[canalUf],
			includeData);

		var includeTopSimples = new IncludeCMS(
			'top-candidatos-simples.js',
			new DustTemplate('top-candidatos-simples.html'),
			'1turno-top-simples-presidente-' + metadata.idAbrangencia,
			[mapaCanaisCargo.presidente],
			includeData);

		var includeTabelaRetratil = new PaginatedCMSInclude(
			'tabela-apuracao-completa-retratil.js',
			new DustTemplate('tabela-apuracao-completa-retratil.html'),
			'1turno-tabela-apuracao-completa-presidente-' + metadata.idAbrangencia,
			[canalUf],
			includeData,
			resultSplitter);

		return [ includeTopCandidato, includeTopSimples, includeTabelaRetratil ];
	}	

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_GOVERNADOR) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeTopSimples = new IncludeCMS(
			'top-governador.js',
			new DustTemplate('top-candidatos-simples.html'),
			'1turno-top-simples-governador-' + metadata.idAbrangencia,
			[canalHome, mapaCanaisCargo.governador],
			includeData);

		var includeTopCandidato = new IncludeCMS(
			'top-4-candidatos-foto.js',
			new DustTemplate('top-4-candidatos-foto.html'),
			'1turno-top-governador-' + metadata.idAbrangencia,
			[canalUf],
			includeData);

		var includeTabelaRetratil = new PaginatedCMSInclude(
			'tabela-apuracao-completa-retratil.js',
			new DustTemplate('tabela-apuracao-completa-retratil.html'),
			'1turno-tabela-apuracao-completa-governador-' + metadata.idAbrangencia,
			[canalUf],
			includeData,
			resultSplitter);

		var includes = [ includeTopSimples, includeTopCandidato, includeTabelaRetratil ];

		var uf = metadata.idAbrangencia;

		if (uf == 'sp' || uf == 'mg' || uf == 'rj') {
			var jsonHome = new IncludeCMS(
				'json-top-3.js',
				new JsonTemplate('handleResponseApuracaoGovernador'),
				'eleicao-home-apuracao-governador-' + metadata.idAbrangencia,
				['eleicao-home-apuracao-governador-' + metadata.idAbrangencia],
				includeData);

			includes.push(jsonHome);			
		}

		return includes;
	}	

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_SENADOR) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeTopSimples = new IncludeCMS(
			'top-senador.js',
			new DustTemplate('top-candidatos-simples.html'),
			'1turno-top-simples-senador-' + metadata.idAbrangencia,
			[canalHome, mapaCanaisCargo.senador],
			includeData);

		var includeTopCandidato = new IncludeCMS(
			'top-4-candidatos-foto.js',
			new DustTemplate('top-4-candidatos-foto.html'),
			'1turno-top-senador-' + metadata.idAbrangencia,
			[canalUf],
			includeData);

		var includeTabelaRetratil = new PaginatedCMSInclude(
			'tabela-apuracao-completa-retratil.js',
			new DustTemplate('tabela-apuracao-completa-retratil.html'),
			'1turno-tabela-apuracao-completa-senador-' + metadata.idAbrangencia,
			[canalUf],
			includeData,
			resultSplitter);

		return [ includeTopSimples, includeTopCandidato, includeTabelaRetratil ];
	}		

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_DEPUTADO_ESTADUAL) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeTopSimples = new IncludeCMS(
			'top-deputado-estadual.js',
			new DustTemplate('top-candidatos-simples.html'),
			'1turno-top-simples-deputado-estadual-' + metadata.idAbrangencia,
			[canalHome, mapaCanaisCargo['deputado-estadual']],
			includeData);

		var includeTopCandidato = new PaginatedCMSInclude(
			'tabela-deputado-estadual-uf.js',
			new DustTemplate('tabela-apuracao-completa.html'),
			'1turno-tabela-apuracao-completa-deputado-estadual-' + metadata.idAbrangencia,
			[canalUf],
			includeData, 
			resultSplitter);

		return [ includeTopSimples, includeTopCandidato ];
	}		

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_DEPUTADO_DISTRITAL) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeTopSimples = new IncludeCMS(
			'top-deputado-estadual.js',
			new DustTemplate('top-candidatos-simples.html'),
			'1turno-top-simples-deputado-estadual-' + metadata.idAbrangencia,
			[canalHome, mapaCanaisCargo['deputado-estadual']],
			includeData);

		var includeTopCandidato = new PaginatedCMSInclude(
			'tabela-deputado-estadual-uf.js',
			new DustTemplate('tabela-apuracao-completa.html'),
			'1turno-tabela-apuracao-completa-deputado-estadual-' + metadata.idAbrangencia,
			[canalUf],
			includeData,
			resultSplitter);

		return [ includeTopSimples, includeTopCandidato ];
	}		

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_DEPUTADO_FEDERAL) {

		var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

		var includeTopSimples = new IncludeCMS(
			'top-deputado-federal.js',
			new DustTemplate('top-candidatos-simples.html'),
			'1turno-top-simples-deputado-federal-' + metadata.idAbrangencia,
			[canalHome, mapaCanaisCargo['deputado-federal']],
			includeData);

		var includeTopCandidato = new PaginatedCMSInclude(
			'tabela-deputado-federal-uf.js',
			new DustTemplate('tabela-apuracao-completa.html'),
			'1turno-tabela-apuracao-completa-deputado-federal-' + metadata.idAbrangencia,
			[canalUf],
			includeData,
			resultSplitter);

		return [ includeTopSimples, includeTopCandidato ];
	}					

	return [];
}

module.exports = CMSIncludeFactory;