var urls = require('../../urls');
var config = require('../../../' + process.argv[2] + '/apurador-cfg.js').config;
var path = require('path');
var Renderer = require('../../../render-engines/DustRenderer');
var FilePublisher = require('../../../FilePublisher');
var publishPageContainer = require('../../publishPageContainer');
var async = require('async');
var logger = winston = require('winston');

var urlHome = urls.urlHome;
var mapaUrlsEstado = urls.mapaUrlsEstado;
var mapaUrlsCargo = urls.mapaUrlsCargo;
var nomesUfUrl = urls.nomesUfUrl;
var nomesUf = urls.nomesUf;
var nomesCargo = urls.nomesCargo;
var nomesCargoCapitalizado = urls.nomesCargoCapitalizado;

logger.remove(winston.transports.Console);
logger.add(winston.transports.Console, {timestamp: true, colorize: true});

var basePath = config.staticPageDestination;

var pages = [
	// home de apuracao
	{
		title: 'Eleições 2014 - Apuração 1º turno - iG',
		description: 'Eleições 2014: acompanhe no iG a apuração do 1º turno das eleições 2014 em tempo real',
		template: '1turno-apuracao-home',
		path: path.join(basePath, 'index.html')
	}
]

for (var uf in mapaUrlsEstado) {
	pages.push({
		title: 'Eleições 2014 - Apuração 1º turno ' + nomesUf[uf] + ' - iG',
		description: 'Eleições 2014: acompanhe no iG a apuração do 1º turno das eleições 2014 em tempo real - ' +
				nomesUf[uf],
		template: '1turno-apuracao-estado',
		path: path.join(basePath, nomesUfUrl[uf], 'index.html'),
		nomeEstado: nomesUf[uf],
		estado: uf
	})
}

for (var cargo in mapaUrlsCargo) {
	pages.push({
		title: 'Eleições 2014 - Apuração 1º turno para ' + nomesCargo[cargo] + ' - iG',
		description: 'Eleições 2014: acompanhe no iG a apuração do ' +
				'1º turno das eleições 2014 em tempo real para ' +
				nomesCargo[cargo],
		template: '1turno-apuracao-' + (cargo == 'presidente' ? 'presidente' : 'cargo'),
		path: path.join(basePath, cargo, 'index.html'),
		cargo: cargo,
		nomeCargo: nomesCargoCapitalizado[cargo]
	})
}

var renderer = new Renderer(path.join(__dirname, '../page-templates'));

renderer.ensureLoadedTemplate('base-page.html');
renderer.ensureLoadedTemplate('1turno-apuracao-home.html');
renderer.ensureLoadedTemplate('1turno-apuracao-cargo.html');
renderer.ensureLoadedTemplate('1turno-apuracao-estado.html');
renderer.ensureLoadedTemplate('1turno-apuracao-presidente.html');

var publisher = new FilePublisher()

async.eachSeries(pages,
		publishPage,
		function () {
			logger.info('Finished publishing pages');
		});

function publishPage (page, cb) {
	publishPageContainer(page, renderer, publisher, cb);
}