var path = require('path');
var url = require('url');
var logger = require('winston');
var async = require('async');
var SingleIncludeRenderOutput = require('../consumidores/includePublishingStrategies/SingleIncludeRenderOutput');
var PaginatedIncludeRenderOutput = require('../consumidores/includePublishingStrategies/PaginatedIncludeRenderOutput');
var FilePublisher = require('../FilePublisher');
var DustTemplate = require('../template-formats/DustTemplate');
var JsonTemplate = require('../template-formats/JsonTemplate');

var INCLUDE_DIR = '_includes';
var VERSIONS_DIR = '_versions';

var VERSION_REGEX = /^(\d\d\d\d)\/(\d\d)\/(\d\d)\/(\d\d)\/(\d\d)\/(\d+)$/;

function IncludePublisher (rootDir, baseUrl, cacheLoader) {
	this._rootDir = rootDir;
	this._baseUrl = baseUrl;
	this._cacheLoader = cacheLoader;
	this._filePublisher = new FilePublisher();
}

IncludePublisher.prototype.publish = function (renderOutput, cb) {
	if (renderOutput instanceof SingleIncludeRenderOutput) {
		this._publishSingleInclude(renderOutput, cb);
	} else {
		this._publishPaginatedInclude(renderOutput, cb);
	}
}

IncludePublisher.prototype.getVersionUrl = function (relativeUrl) {
	var versionUrl = url.parse(this._baseUrl);
	versionUrl.pathname = path.join(versionUrl.pathname,
	 		relativeUrl);

	var versionUrlStr = url.format(versionUrl);

	logger.info('Calculated version url', versionUrlStr);

	return versionUrlStr;
}

IncludePublisher.prototype._publishSingleInclude = function (renderOutput, cb) {
	var self = this;
	var baseName = renderOutput.include.caminho;

	var versionPath = path.join(
			VERSIONS_DIR, 
			baseName,
			this._buildPathFromVersion(renderOutput.include.version),
			this._buildFileName(renderOutput.include, null, false));

	var paths = [
			// Include
			path.join(this._rootDir, INCLUDE_DIR, 
					this._buildFileName(renderOutput.include, null, true)),

			// Version
			path.join(this._rootDir, versionPath)
			];

	async.eachSeries(paths,
			function (path, cb) {
				self._filePublisher.publish(path, renderOutput.output, cb)
			},
			function (err) {
				if (!err) {
					self._loadCache(versionPath)	
				}
				cb();
			});
}

IncludePublisher.prototype._loadCache = function (relativePath) {
	this._cacheLoader.loadInclude(this.getVersionUrl(relativePath));
}

IncludePublisher.prototype._publishPaginatedInclude = function (renderOutput, cb) {
	var self = this;
	var baseName = renderOutput.include.caminho;
	var page = renderOutput.pageData.pagination.currentPage;

	var versionPath = path.join(
			VERSIONS_DIR, 
			baseName,
			this._buildPathFromVersion(renderOutput.include.version),
			this._buildFileName(renderOutput.include, page, false));

	var paths = [
			// Version
			path.join(this._rootDir, versionPath)
			];

	if (page == 1) {
		// Include
		paths.push(path.join(this._rootDir, INCLUDE_DIR, 
					this._buildFileName(renderOutput.include, page, true)));
	}

	async.eachSeries(paths,
			function (path, cb) {
				self._filePublisher.publish(path, renderOutput.output, cb)
			},
			function (err) {
				if (!err) {
					self._loadCache(versionPath)	
				}
				cb();
			});
}

IncludePublisher.prototype._getExtension = function (include, isInclude) {
	if (include.template instanceof JsonTemplate) {
		return 'json';

	} else if (include.template instanceof DustTemplate) {
		if (isInclude) {
			return 'shtml';
		} else  {
			return 'html';
		}
	} else {
		throw new Error('Cannot define extension for unknown template format',
				include.template)
	}
}

IncludePublisher.prototype._buildFileName = function (include, page, isInclude) {
	var name = include.caminho;

	if (page !== null) {
		name += '.' + page;
	}

	name += '.' + this._getExtension(include, isInclude);

	return name;
}

IncludePublisher.prototype._buildPathFromVersion = function (version) {
	var ext = VERSION_REGEX.exec(version);
	if (!ext) {
		logger.error('Invalid version', version);
		return '__invalid_version';
	}

	return ext[0];
}

module.exports = IncludePublisher;