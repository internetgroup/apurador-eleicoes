var async = require('async');
var logger = require('winston');
var DustTemplate = require('../template-formats/DustTemplate');

function publishPage (page, renderer, publisher, cb) {
	logger.info('Publishing page', page.path);

	async.waterfall([
			function (cb) {
				renderer.render(
						new DustTemplate(page.template + '.html'),
						page,
						cb)				
			},
			function (conteudoOutput, cb) {
				var pageData = Object.create(page);
				pageData.conteudoHTML = conteudoOutput;
				renderer.render(
						new DustTemplate('base-page.html'),
						pageData,
						cb)	
			},
			function (htmlOutput, cb) {
				publisher.publish(
						page.path,
						htmlOutput,
						cb)
			}
		],
		function (err) {
			if (err) {
				logger.info('Failed to publish page', page.path, err.stack || err);
				cb(err);
				return;
			}
			logger.info('Successfully published page', page.path);
			cb();
		})
}

module.exports = publishPage;