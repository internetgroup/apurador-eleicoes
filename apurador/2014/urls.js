var Classificador = require('../ClassificadorArquivos');
var config = require('../' + process.argv[2] + '/apurador-cfg.js').config;

var nomeTurno = config.turno + 'turno';

var baseUrlPath = '/eleicoes/apuracao/2014/' + nomeTurno;

var nomesUf = {
	'sp': 'São Paulo',
	'rj': 'Rio de Janeiro', 
	'mg': 'Minas Gerais', 
	'es': 'Espírito Santo', 
	'df': 'Distrito Federal',
	'rs': 'Rio Grande do Sul', 
	'pr': 'Paraná', 
	'sc': 'Santa Catarina',
	'ba': 'Bahia', 
	'ce': 'Ceará', 
	'pe': 'Pernambuco', 
	'ma': 'Maranhão', 
	'pi': 'Piauí', 
	'rn': 'Rio Grande do Norte', 
	'al': 'Alagoas', 
	'pb': 'Paraíba', 
	'se': 'Sergipe',
	'go': 'Goiás', 
	'ms': 'Mato Grosso do Sul', 
	'mt': 'Mato Grosso',
	'am': 'Amazonas', 
	'pa': 'Pará', 
	'ro': 'Rondônia', 
	'ap': 'Amapá', 
	'ac': 'Acre', 
	'rr': 'Roraima', 
	'to': 'Tocantins'	
}

var nomesUfUrl = {
	'sp': 'sao-paulo',
	'rj': 'rio-de-janeiro', 
	'mg': 'minas-gerais', 
	'es': 'espirito-santo', 
	'df': 'distrito-federal',
	'rs': 'rio-grande-do-sul', 
	'pr': 'parana', 
	'sc': 'santa-catarina',
	'ba': 'bahia', 
	'ce': 'ceara', 
	'pe': 'pernambuco', 
	'ma': 'maranhao', 
	'pi': 'piaui', 
	'rn': 'rio-grande-do-norte', 
	'al': 'alagoas', 
	'pb': 'paraiba', 
	'se': 'sergipe',
	'go': 'goias', 
	'ms': 'mato-grosso-do-sul', 
	'mt': 'mato-grosso',
	'am': 'amazonas', 
	'pa': 'para', 
	'ro': 'rondonia', 
	'ap': 'amapa', 
	'ac': 'acre', 
	'rr': 'roraima', 
	'to': 'tocantins'
}

var urlHome = baseUrlPath + '/';

var urlsEstado = [];
var mapaUrlsEstado = {};
for (var uf in nomesUfUrl) {
	var url = baseUrlPath + '/' + nomesUfUrl[uf] + '/';
	urlsEstado.push(url);
	mapaUrlsEstado[uf] = url;
}

var mapaUrlsCargo = {
	presidente: baseUrlPath + '/' + 'presidente/',
	governador: baseUrlPath + '/' + 'governador/',
	senador: baseUrlPath + '/' + 'senador/',
	'deputado-estadual': baseUrlPath + '/' + 'deputado-estadual/',
	'deputado-federal': baseUrlPath + '/' + 'deputado-federal/'
}

var urlsCargo = [];
for (var cargo in mapaUrlsCargo) {
	urlsCargo.push(mapaUrlsCargo[cargo]);
}

var canalHome = nomeTurno;

var canaisEstado = [];
var mapaCanaisEstado = {};

for (var uf in nomesUfUrl) {
	var canal = nomeTurno + '-' + nomesUfUrl[uf];
	canaisEstado.push(canal);
	mapaCanaisEstado[uf] = canal;
}

var mapaCanaisCargo = {
	presidente: nomeTurno + '-presidente',
	governador: nomeTurno + '-governador',
	senador: nomeTurno + '-senador',
	'deputado-estadual': nomeTurno + '-deputado-estadual',
	'deputado-federal': nomeTurno + '-deputado-federal'
}

var canaisCargo = [];
for (var cargo in mapaCanaisCargo) {
	canaisCargo.push(mapaCanaisCargo[cargo]);
}

var nomesCargo = {
	presidente: 'presidente',
	governador: 'governador',
	senador: 'senador',
	'deputado-estadual': 'deputado estadual',
	'deputado-federal': 'deputado federal'
}

var nomesCargoImagem = {};
nomesCargoImagem[Classificador.CARGO_PRESIDENTE] = 'presidente';
nomesCargoImagem[Classificador.CARGO_GOVERNADOR] = 'governador';
nomesCargoImagem[Classificador.CARGO_SENADOR] = 'senador';
nomesCargoImagem[Classificador.CARGO_DEPUTADO_FEDERAL] = 'deputado-federal';
nomesCargoImagem[Classificador.CARGO_DEPUTADO_ESTADUAL] = 'deputado-estadual';
// Deputado distrital eh tratado como estadual:
nomesCargoImagem[Classificador.CARGO_DEPUTADO_DISTRITAL] = 'deputado-estadual';

var nomesCargoCapitalizado = {
	presidente: 'Presidente',
	governador: 'Governador',
	senador: 'Senador',
	'deputado-estadual': 'Deputado Estadual',
	'deputado-federal': 'Deputado Federal'
}

module.exports = {
	canalHome: canalHome,
	canaisEstado: canaisEstado,
	mapaCanaisEstado: mapaCanaisEstado,
	canaisCargo: canaisCargo,
	mapaCanaisCargo: mapaCanaisCargo,
	nomesUf: nomesUf,
	nomesCargo: nomesCargo,
	nomesCargoImagem: nomesCargoImagem,
	nomesUfUrl: nomesUfUrl,
	urlHome: urlHome,
	urlsEstado: urlsEstado,
	mapaUrlsEstado: mapaUrlsEstado,
	urlsCargo: urlsCargo,
	mapaUrlsCargo: mapaUrlsCargo,
	nomesCargoCapitalizado: nomesCargoCapitalizado
}
