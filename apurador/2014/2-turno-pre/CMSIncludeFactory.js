var Classificador = require('../../ClassificadorArquivos');
var IncludeCMS = require('../../consumidores/IncludeCMS');
var PaginatedCMSInclude = require('../../consumidores/PaginatedCMSInclude');
var ResultSplitter = require('../ResultSplitter');
// TODO: change this to a better solution
var config = require('../../' + process.argv[2] + '/apurador-cfg.js').config;
var IncludePaginator = require('../../consumidores/IncludePaginator');
var util = require('util');
var urls = require('../urls');
var DustTemplate = require('../../template-formats/DustTemplate');
var JsonTemplate = require('../../template-formats/JsonTemplate');

var canalHome = urls.canalHome;
var canaisCargo = urls.canaisCargo;
var mapaCanaisEstado = urls.mapaCanaisEstado;
var mapaCanaisCargo = urls.mapaCanaisCargo;

var resultSplitter = new ResultSplitter(10);

function CMSIncludeFactory () {}

CMSIncludeFactory.prototype.makeIncludesForData = function (metadata, dadosFixos, dadosVariaveis) {
	var rootDir = config.publishDestination;

	var includeData = {
			metadata: metadata,
			dadosFixos: dadosFixos,
			dadosVariaveis: dadosVariaveis,
			toString: function () {return metadata.nomeArquivo}
		};

	if (metadata.tipoAbrangencia == 'uf' && 
			metadata.cargo == Classificador.CARGO_GOVERNADOR) {

		var uf = metadata.idAbrangencia;
		if (['sp', 'mg', 'es', 'pr', 'sc', 'mt', 'ma', 'pi', 'pe', 'al', 'se', 'ba', 'to'].indexOf(uf) > -1) {
			var canalUf = mapaCanaisEstado[metadata.idAbrangencia];

	        /*
	         * Include utilizado nas paginas de estado (do 2o. turno) 
	         * com governador ja eleito no 1o. turno
	         */ 
	        var includeCandidatoEleito = new IncludeCMS(
	          '2turno-top-governador-eleito.js',
	          new DustTemplate('2turno-top-governador-eleito.html'),
	          '2turno-top-governador-' + metadata.idAbrangencia,
	          [],
	          includeData);

			return [includeCandidatoEleito];
		}
	}	

	return [];
}

module.exports = CMSIncludeFactory;
