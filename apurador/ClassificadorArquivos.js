function Classificador () {}

Classificador.CARGO_PRESIDENTE = '0001';
Classificador.CARGO_GOVERNADOR = '0003';
Classificador.CARGO_SENADOR = '0005';
Classificador.CARGO_DEPUTADO_FEDERAL = '0006';
Classificador.CARGO_DEPUTADO_ESTADUAL = '0007';
Classificador.CARGO_DEPUTADO_DISTRITAL = '0008';
Classificador.CARGO_PREFEITO = '0011'; 
Classificador.CARGO_VEREADOR = '0013';

var PADRAO_FIXO = /^([a-z]{2})(\d{5})?-(\d{4})-e\d{6}-f(\d{3})$/;
var PADRAO_VAR = /^([a-z]{2})(\d{5})?-(\d{4})-e\d{6}-v$/;

Classificador.classificar = function (nome) {
	var cla = classificarFixo(nome);

	if (!cla)
		cla = classificarVariavel(nome);

	if (!cla) {
		cla = criarClassificacaoVazia(nome);
	}

	return cla;
}

function classificarFixo (nome) {
	var mat = PADRAO_FIXO.exec(nome);
	if (mat) {
		var classificacao = criarClassificacaoVazia(nome);

		classificacao.tipo = 'fixo';
		classificacao.versao = mat[4];
		classificarAbrangencia(mat[1], mat[2], classificacao);
		classificarCargoEAcompanhamento(mat[3], classificacao);

		return classificacao;
	}
	return null;
}

function classificarVariavel (nome) {
	var mat = PADRAO_VAR.exec(nome);
	if (mat) {
		var classificacao = criarClassificacaoVazia(nome);

		classificacao.tipo = 'variavel';
		classificarAbrangencia(mat[1], mat[2], classificacao);
		classificarCargoEAcompanhamento(mat[3], classificacao);

		return classificacao;
	}
	return null;
}

function criarClassificacaoVazia (nome) {
	return {
		tipo: null,				// fixo|variavel
		nomeArquivo: nome,		//
		tipoAbrangencia: null,	// br|zz|uf|municipio
		isAcompanhamento: null,	// boolean
		cargo: null,			// 0000
		idAbrangencia: null,	// br|zz|nome da uf|id municipio
		idAbrangenciaPai: null,	// nome da uf quando abrangencia eh municipio
		versao: null			// para arquivos fixos
	}
}

function classificarAbrangencia (abrangencia, idMunicipio, classificacao) {
	var abrangencia = abrangencia.toLowerCase();

	if (abrangencia == 'br') {
		classificacao.tipoAbrangencia = 'br';
		classificacao.idAbrangencia = 'br';

	} else if (abrangencia == 'zz') {
		classificacao.tipoAbrangencia = 'zz'
		classificacao.idAbrangencia = 'zz';

	} else if (typeof idMunicipio != 'undefined' && idMunicipio !== null) {
		classificacao.tipoAbrangencia = 'municipio'
		classificacao.idAbrangencia = idMunicipio;
		classificacao.idAbrangenciaPai = abrangencia;

	} else {
		classificacao.tipoAbrangencia = 'uf'
		classificacao.idAbrangencia = abrangencia;
	}	
}

function classificarCargoEAcompanhamento (cargoOuAcompanhamento, classificacao) {
	classificacao.isAcompanhamento = (cargoOuAcompanhamento == '0000');
	if (!classificacao.isAcompanhamento) {
		classificacao.cargo = cargoOuAcompanhamento;
	}
}

module.exports = Classificador;