var dust = require('dustjs-linkedin');
var dustHelpers = require('dustjs-helpers');
var path = require('path');
var logger = require('winston');
var fs = require('fs');
var utils = require('../utils').utils;

function Renderer (templateDir, templates) {
	if (!(this instanceof Renderer))
		return new Renderer(templateDir, templates)

	this._templateDir = templateDir;
	this._templates = templates;

	this._compileTemplates();
}

Renderer.prototype._compileTemplates = function () {
	var templateDir = this._templateDir;
	var templates = this._templates;
	var compiled = {}

	for (var templateName in templates) {
		this.ensureLoadedTemplate(templateName);
	}
}

Renderer.prototype.ensureLoadedTemplate = function (templateName) {
	var templatePath = path.join(this._templateDir, templateName);
	logger.info('Loading template', templateName,
			'from file', templatePath);
	var contents = getFileContents(templatePath);

	compileAndRegisterTemplate(templateName, contents);	
}

Renderer.prototype.render = function (template, data, cb) {
	logger.info('Rendering template', template)
    this.ensureLoadedTemplate(template.templateName);    
	dust.render(template.templateName, data, function (err, out) {
		if (err) {
			logger.error('Failed to render template', template.templateName,
				'with data', data,
				err.stack || err);
		}
		cb(err, out)
	})
}

function getFileContents (filePath) {
    var contents;
    try {
        contents = fs.readFileSync(filePath, 'utf8');    
    } catch (e) {
        logger.error('Error reading file', filePath, 
        		e.stack || e);
        process.exit();        
    }

	return contents;	
}

function compileAndRegisterTemplate (name, source) {
	logger.info('Compiling template', name);
	var compiled = dust.compile(source, name);
	logger.info('Registering template', name);
	dust.loadSource(compiled);
}

dust.optimizers.format = function (ctx, node) { return node }

/*
 * Dust helpers
 */

if (!dust.helpers)
    dust.helpers = {};
 
dust.helpers['br-sep-000'] = function(chunk, context, bodies, params) {
    if (bodies.block) {
        return chunk.capture(bodies.block, context, function(number, chunk) {
        	var number = number + '';

		    x = number.split('.');
		    x1 = x[0];
		    x2 = x.length > 1 ? ',' + x[1] : '';
		    var rgx = /(\d+)(\d{3})/;
		    while (rgx.test(x1)) {
		            x1 = x1.replace(rgx, '$1' + '.' + '$2');
		    }
            chunk.end(x1 + x2);
        });
    }	
    return chunk;
};

dust.helpers['br-dec-1'] = function(chunk, context, bodies, params) {
    if (bodies.block) {
        return chunk.capture(bodies.block, context, function(number, chunk) {
    		chunk.end(utils.formatBr1Decimal(number));
        });
    }	
    return chunk;
};

dust.helpers['date-dmyyyy'] = function(chunk, context, bodies, params) {
    if (bodies.block) {
        return chunk.capture(bodies.block, context, function(strDate, chunk) {
        	var date = new Date(strDate);
        	var formatted = date.getDate() + '/' + 
        			(date.getMonth() + 1) + '/' + 
        			 date.getFullYear();
            chunk.end(formatted);
        });
    }	
    return chunk;
};

dust.helpers['html-br'] = function(chunk, context, bodies, params) {
    if (bodies.block) {
        return chunk.capture(bodies.block, context, function(text, chunk) {
            chunk.end(text.replace(/\r|\n|\r\n/g, '<br>'));
        });
    }	
    return chunk;
};

module.exports = Renderer;