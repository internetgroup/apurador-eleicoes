var path = require('path');
var logger = require('winston');

function Renderer () {
	if (!(this instanceof Renderer))
		return new Renderer()
}

Renderer.prototype.render = function (template, data, cb) {
	logger.info('Rendering JSON', template)
    var output = JSON.stringify(data);
    if (typeof template.jsonpCallbackName != 'undefined') {
        output = template.jsonpCallbackName + '(' + output + ')'
    }
    process.nextTick(function () {
        cb(null, output);
    });
}

module.exports = Renderer;