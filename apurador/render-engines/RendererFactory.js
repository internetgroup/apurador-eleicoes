var DustTemplate = require('../template-formats/DustTemplate');
var JsonTemplate = require('../template-formats/JsonTemplate');
var DustRenderer = require('./DustRenderer');
var JsonRenderer = require('./JsonRenderer');

function RendererFactory (config) {
	this._dustRenderer = new DustRenderer(config.templateDir, []);
	this._jsonRenderer = new JsonRenderer();
}

RendererFactory.prototype.getRendererForTemplate = function (template) {
	if (template instanceof DustTemplate) {
		return this._dustRenderer;
	} else if (template instanceof JsonTemplate) {
		return this._jsonRenderer;
	} else {
		throw new Error('Unknown template format', template);
	}
}

module.exports = RendererFactory;