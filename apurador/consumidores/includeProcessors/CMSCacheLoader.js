var logger = require('winston');
var request = require('request');
var async = require('async');
var url = require('url')

function CMSCacheLoader (cacheServers) {
	this._cacheServers = cacheServers;
}

CMSCacheLoader.prototype.loadInclude = function (includeUrl) {
	logger.info('Loading include into cache at URL ', includeUrl);

	var self = this;

	for (var i = 0; i < this._cacheServers.length; i++) {
		self._loadOnServer(this._cacheServers[i], includeUrl);
	}
}

CMSCacheLoader.prototype._loadOnServer = function (server, includeUrl) {
	var parsedUrl = url.parse(includeUrl);

	var realHost = parsedUrl.host;

	parsedUrl.host = server;

	var options = {
		url: url.format(parsedUrl),
		followRedirect: false,
		headers: {
			'Host': realHost
		}
	}

	logger.info('Requesting cache load:', options);

	request(options, function (err, response) {
			if (err) {
				logger.error('Error loading include into cache at URL', includeUrl,
						err.stack || err);
				return;
			}

			if (response.statusCode < 200 || response.statusCode >= 300) {
				logger.error('Error loading include into cache at URL', includeUrl,
						'- returned status code',
						response.statusCode);
				return;			
			}

			logger.info('Successfully loaded include into cache at URL', includeUrl,
					'- returned status code', response.statusCode);
		});
}

module.exports = CMSCacheLoader;