var logger = require('winston');
var RTClient = require('../../RTClient');
var crypto = require('crypto');
var EventEmitter = require('events').EventEmitter;
var util = require('util');

function RealtimeSender (config) {
	var rtClient = new RTClient(config);
	var self = this;

	rtClient.onConnected = function () {
		logger.info('Connected to ORTC');
		self.emit('ortcConnected', {});
	}

	this._rtClient = rtClient;
	this._config = config;

	this._setupAuth();
}

util.inherits(RealtimeSender, EventEmitter);

RealtimeSender.prototype._setupAuth = function () {
	var self = this;
	var authRenew = parseInt(this._config.authRenew) || 30;

	// setInterval(function () {
	// 	self._authenticate(function () {});
	// }, authRenew * 1000);

	// this._authenticate(function (err) {
	// 	if (!err) {
		    self._rtClient.setUp();
		    self._rtClient.connect();
	// 	}
	// });	
	// 
	// self._rtClient.onConnected = function () {
	// 	console.log('xXSSSSSSSSSS@@@)@)@)@)@)@)@)@)@)');
	// 	self._rtClient.sendMessage('eleicoes2014:teste222', 
	// 			JSON.stringify( { version: 'aaaa' } ))
	// }
}

RealtimeSender.prototype._authenticate = function (cb) {
	this._rtClient.authenticateChannel(this._config.channelPrefix + '*',
			function (err) {
				if (err) {
					logger.error('Could not authenticate on ORTC:', err.stack || err);
				}
				cb(err);
			});
}

RealtimeSender.prototype.process = function (includeCMS, cb) {
	logger.info('Sending realtime update for include', includeCMS.toString());

	var channels = includeCMS.urlsPaginas;

	for (var i = 0; i < channels.length; i++) {
		logger.info('Sending RT update to channel',
				channels[i],
				'for component', includeCMS.caminho, 
				'with version' + includeCMS.version);

		this._rtClient.sendMessage(this._config.channelPrefix + channels[i], 
			JSON.stringify( { component: includeCMS.caminho, version: includeCMS.version } ));
	}

	process.nextTick(cb);
}

module.exports = RealtimeSender;