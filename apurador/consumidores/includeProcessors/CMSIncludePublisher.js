var logger = require('winston');

function CMSIncludePublisher (renderer, publisher) {
	this._renderer = renderer;
	this._publisher = publisher;
}

CMSIncludePublisher.prototype.process = function (includeCMS, cb) {
	logger.info('Publishing include', includeCMS.toString());

	var self = this;

	this._renderer.render(includeCMS.templateName, 
			includeCMS.logicName, 
			includeCMS.dados, 
			function (err, output) {
				if (err) {
					logger.error('Failed to publish include', err.stack || err);
					cb(err);
					return;
				}

				self._publisher.publicar(
						includeCMS.caminho,
						output,
						cb,
						function (err) {
							logger.error('Failed to publish include', err.stack || err);
							cb(err);
						}
					)
	})
}

module.exports = CMSIncludePublisher;