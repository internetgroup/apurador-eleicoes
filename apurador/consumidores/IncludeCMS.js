var utils = require('../utils').utils;

function CMSInclude (logicName, template, caminho, urlsPaginas, dados) {
	this.logicName = logicName;
	this.template = template;
	this.caminho = caminho;
	this.urlsPaginas = urlsPaginas;
	this.dados = dados;
	this.version = utils.createVersion();
}

CMSInclude.prototype.toString = function () {
	return '[' + this.template.toString() + ', ' + this.caminho + ']';
}

module.exports = CMSInclude;