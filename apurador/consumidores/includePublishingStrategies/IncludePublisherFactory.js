var SingleIncludePublishingStrategy = require('./SingleIncludePublishingStrategy');
var PaginatedIncludePublishingStrategy = require('./PaginatedIncludePublishingStrategy');
var IncludeCMS = require('../IncludeCMS');
var PaginatedCMSInclude = require('../PaginatedCMSInclude');

function IncludePublisherFactory (logicProcessor, renderer, publisher) {
	this._singleStrategy = new SingleIncludePublishingStrategy(logicProcessor, renderer, publisher);
	this._paginatedStrategy = new PaginatedIncludePublishingStrategy(logicProcessor, renderer, publisher);
}

IncludePublisherFactory.prototype.getPublisherForInclude = function (include) {
	if (include instanceof IncludeCMS) {
		return this._singleStrategy;
	} else if (include instanceof PaginatedCMSInclude) {
		return this._paginatedStrategy;
	} else {
		throw new Error('Unknown include type', include.toString());
	}
}

module.exports = IncludePublisherFactory;