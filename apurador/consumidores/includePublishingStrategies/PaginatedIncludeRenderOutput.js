function PaginatedIncludeRenderOutput (include, pageData, output) {
	this.include = include;
	this.pageData = pageData;
	this.output = output;
}

module.exports = PaginatedIncludeRenderOutput;