var logger = require('winston');
var async = require('async');
var util = require('util');
var PaginatedIncludeRenderOutput = require('./PaginatedIncludeRenderOutput');

function PaginatedIncludePublishingStrategy (logicProcessor, renderEngineFactory, publisher) {
	this._logicProcessor = logicProcessor;
	this._renderEngineFactory = renderEngineFactory;
	this._publisher = publisher;
}

PaginatedIncludePublishingStrategy.prototype.process = function (include, cb) {
	var includeDesc = include.toString();

	logger.info('Publishing paginated include', includeDesc);

	var self = this;

	async.waterfall([
			function (cb) {
				logger.info('Running logic for include', includeDesc);
				self._logicProcessor.processLogic(include.logicName, include.dados, cb)
			},

			function (processedData, cb) {
				logger.info('Splitting include data into pages for include', includeDesc);

				var splittedData = include.splitter.split(processedData);
				process.nextTick(function () {
					cb(null, splittedData);
				})
			},

			function (splittedData, cb) {
				self._processPages(include, splittedData, cb)
			}
		],
		cb);
}

PaginatedIncludePublishingStrategy.prototype._processPages = function (include, pages, cb) {	
	var self = this;

	var renderEngine = this._renderEngineFactory.getRendererForTemplate(include.template);

	async.forEachSeries(pages, function (page, cb) {
		var pageNumber = page.pagination.currentPage;

		logger.info('Publishing page', pageNumber, 
				'from include', include.toString());

		renderEngine.render(include.template, page,
			function (err, output) {
				if (err) {
					logger.error('Failed to publish page', pageNumber, 
							'from include', include.toString(), 
							err.stack || err);
					cb(err);
					return;
				}

				var renderOutput = new PaginatedIncludeRenderOutput(include, page, output)

				self._publisher.publish(
						renderOutput,
						function (err) {
							if (err) {
								logger.error('Failed to publish include', err.stack || err);	
							}
							cb(err);
						}
					)
		})
	},
	cb)
}

module.exports = PaginatedIncludePublishingStrategy;