var logger = require('winston');
var async = require('async');
var util = require('util');
var SingleIncludeRenderOutput = require('./SingleIncludeRenderOutput');

function SingleIncludePublishingStrategy (logicProcessor, renderEngineFactory, publisher) {
	this._logicProcessor = logicProcessor;
	this._renderEngineFactory = renderEngineFactory;
	this._publisher = publisher;
}

SingleIncludePublishingStrategy.prototype.process = function (include, cb) {
	var includeDesc = include.toString();

	logger.info('Publishing single include', includeDesc);

	var self = this;

	async.waterfall([
			function (cb) {
				logger.info('Running logic for include', includeDesc);
				self._logicProcessor.processLogic(include.logicName, include.dados, cb)
			},

			function (processedData, cb) {
				self._processPage(include, processedData, cb)
			}
		],
		cb);
}

SingleIncludePublishingStrategy.prototype._processPage = function (include, processedData, cb) {	
	var self = this;

	var renderEngine = this._renderEngineFactory.getRendererForTemplate(include.template);

	renderEngine.render(include.template, processedData,
		function (err, output) {
			if (err) {
				logger.error('Failed to publish include', include.toString(), 
						err.stack || err);
				cb(err);
				return;
			}

			var renderOutput = new SingleIncludeRenderOutput(include, output);

			self._publisher.publish(
					renderOutput,
					function (err) {
						if (err) {
							logger.error('Failed to publish include', err.stack || err);	
						}
						cb(err);
					}
				)
	})
}

module.exports = SingleIncludePublishingStrategy;