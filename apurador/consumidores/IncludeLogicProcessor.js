var path = require('path');
var logger = require('winston');

function IncludeLogicProcessor (templateDir) {
	this._templateDir = templateDir;
	this._loadedLogic = {};
}

IncludeLogicProcessor.prototype.processLogic = function (logicName, data, cb) {
	logger.info('Running logic ', logicName);

	try {
		var logic = this._ensureLoadedLogic(logicName);		
	} catch (err) {
		process.nextTick(function () { 
			cb(err) 
		});
		return;
	}

	var processedData = this._preProcessData(data, logic);

	process.nextTick(function () { 
		cb(null, processedData) 
	});
}

IncludeLogicProcessor.prototype._ensureLoadedLogic = function (logicName) {
	if (!this._loadedLogic[logicName]) {
		try {
			var logic = require(path.join(this._templateDir, logicName));	
		} catch (err) {
			logger.error('Error loading template logic for template',
					logicName,
					err.stack || err)
			throw err;
		}
		this._loadedLogic[logicName] = logic;
	}	
	return this._loadedLogic[logicName];
}

IncludeLogicProcessor.prototype._preProcessData = function (data, logicClass) {
	var logic = Object.create(logicClass.prototype);

	var deepCopy = JSON.parse(JSON.stringify(data));

	return logic.preProcessData(deepCopy);
}

module.exports = IncludeLogicProcessor;