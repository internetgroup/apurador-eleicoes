var CMSIncludePublisher = require('./includeProcessors/CMSIncludePublisher');
var RealtimeSender = require('./includeProcessors/RealtimeSender');
var RendererFactory = require('../render-engines/RendererFactory');
var LogicProcessor = require('./IncludeLogicProcessor');
var async = require('async');
var logger = require('winston');
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var IncludePublisherFactory = require('./includePublishingStrategies/IncludePublisherFactory');
// TODO: change this to a better solution
var config = require('../' + process.argv[2] + '/apurador-cfg.js').config;

function IncludeBasedConsumerStrategy (includeFactory, publisher) {
	var self = this;

	this._includeFactory = includeFactory;

	// Absolute path required
	var templateDir = config.templateDir;
	var templateRendererFactory = new RendererFactory(config);
	var logicProcessor = new LogicProcessor(templateDir);

	this._includePublisherFactory = new IncludePublisherFactory(logicProcessor, 
				templateRendererFactory, publisher);

	this._realtimeSender = new RealtimeSender(config.ortc);

	this._realtimeSender.once('ortcConnected', function () {
		self.emit('ready', {});
	})
}

util.inherits(IncludeBasedConsumerStrategy, EventEmitter);

IncludeBasedConsumerStrategy.prototype.consume = function (
		metadata, dadosFixos, dadosVariaveis, onsuccess, onfailure) {

	logger.info('Handling includes for file', metadata.nomeArquivo);

	var includes = this._includeFactory.makeIncludesForData(
			metadata, dadosFixos, dadosVariaveis);

	var self = this;

	var includeNames = [];
	for (var i = 0; i < includes.length; i++)
		includeNames.push(includes[i].templateName);

	logger.info('Found', includes.length, 'includes to be generated from file',
			metadata.nomeArquivo + ':',
			includeNames.join(', '));

	async.forEachSeries(includes, 
		function (include, cb) {
			self._processInclude(include, cb);
		},
		onsuccess);
}

IncludeBasedConsumerStrategy.prototype._processInclude = function (include, cb) {
	var self = this;

	logger.info('Processing include', include.toString());

	include.dados.include = {name: include.caminho, version: include.version};

	var publisher = this._includePublisherFactory.getPublisherForInclude(include);

	async.series([
			function (cb) {
				publisher.process(include, cb);
			},

			function (cb) {
				self._realtimeSender.process(include, cb);
			}
		],

		function (err) {
			if (err) {
				logger.error('Failed to process include', include.toString(), err.stack || err);
				return;
			}
			logger.info('Done processing include', include.toString());
			cb();
		})
}

module.exports = IncludeBasedConsumerStrategy;