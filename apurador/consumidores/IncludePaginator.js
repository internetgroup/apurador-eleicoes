var PaginatedCMSInclude = require('./PaginatedCMSInclude');
var util = require('util');
var logger = require('winston');

function IncludePaginator () {}

IncludePaginator.prototype.createPaginatedInclude = function (
		logicName, template, caminho, urlsPaginas, includeData, pageLength) {
	logger.info('Creating paginated include from data with page size', pageLength);

	var include = new PaginatedCMSInclude(
			logicName, 
			template, 
			caminho,
			urlsPaginas,
			pages);

	logger.info('Generated paginated include', include.toString());

	return include;
}

module.exports = IncludePaginator;