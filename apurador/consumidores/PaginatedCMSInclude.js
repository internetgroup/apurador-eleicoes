var utils = require('../utils').utils;

function PaginatedCMSInclude (logicName, template, caminho, urlsPaginas, dados, splitter) {
	this.logicName = logicName;
	this.template = template;
	this.caminho = caminho;
	this.urlsPaginas = urlsPaginas;
	this.dados = dados;
	this.splitter = splitter;
	this.version = utils.createVersion();	
}

PaginatedCMSInclude.prototype.toString = function () {
	return '[' + this.template.toString() + ', ' + this.caminho + ']';
}

module.exports = PaginatedCMSInclude;