/*

http://www.tse.jus.br/eleicoes/eleicoes-2014/parceria-divulgacao-resultados-2014

Simulado com os Parceiros (02, 03, 04, 09, 10, 11, 16, 17 e 18/09): evento onde são disponibilizados 
dados simulados para os parceiros testarem suas aplicações. Um ciclo de dados será disponibilizado 
pela manhã (iniciando às 10h) e o mesmo novamente à tarde (iniciando às 15h). O código da eleição para
 o simulado será 4464 e a url para conexão http://parceiros.tse.jus.br/, ambiente simulado 
http://parceiros.tse.jus.br/2014/divulgacao/simulado/4464/distribuicao/pr/pr-0000-e044641-v.zip

 http://parceiros.tse.jus.br/2014/divulgacao/simulado/4464/distribuicao/br/br-e044641-i.zip

Mais informações podem ser encontradas na apresentação técnica para os parceiros e nos outros arquivos 
disponibilizados na área de Documentos. .:: NOVO ::.

*/

var config = {
	// host: 'parceiros.tse.jus.br',
	host: 'localhost',
	porta: 8080,
	ano: '2014',
	// eleicaoID: '4464', //'9999',
	// eleica: '044641', //'099993', // 2014 turno1 001431, turno2 001441
	// eleicaoID: '143', //'9999',
	eleicaoID: '144', //'9999',
	// eleica: '001431', //'099993', // 2014 turno1 001431, turno2 001441
	eleica: '001441', //'099993', // 2014 turno1 001431, turno2 001441
	ambiente: 'oficial',
	// ambiente: 'simulado',
	portaProxyTSE: 8096,

	turno: 2,

	// // host: 'parceiros.tse.jus.br',
	// host: 'localhost',
	// porta: 8080,
	// ano: '2014',
	// eleicaoID: '4464', //'9999',
	// eleica: '044641', //'099993', // 2014 turno1 001431, turno2 001441
	// ambiente: 'simulado',
	// portaProxyTSE: 8096,

	mongo: {
		host: 'localhost',
		port: 27017,
		db: 'eleicoes2014_local_2turno' //_dev'
	},

	instanciaID: 'main_ufs',

	abrangencias: [
		'br',
		'df', 'pr', 
		'sp', 'rj', 'mg', 'es', 'df',
		'rs', 'pr', 'sc',
		'ba', 'ce', 'pe', 'ma', 'pi', 'rn', 'al', 'pb', 'se',
		'go', 'ms', 'mt',
		'am', 'pa', 'ro', 'ap', 'ac', 'rr', 'to'
	],

	ignorarTodosMunicipios: true,

	baseUrl: 'http://localhost/',
	cacheServers: [
		'cms-cache-mid-3.aws.ig.com.br',
		'cms-cache-mid-4.aws.ig.com.br'
	],

	templateDir: '/var/www/apurador-eleicoes/apurador/2014/templates',
	publishDestination: '/var/www/eleicoes2014/2turno',
	staticPageDestination: '/var/www/eleicoes2014/2turno',

	// apenasMunicipios: {
	// 	'57053': 1,
	// 	'71072': 1, '60011': 1, '41238': 1, '57053': 1,
	// 	'88013': 1, '75353': 1, '81051': 1,
	// 	'38490': 1, '13897': 1, '25313': 1, '09210': 1, '12190': 1, '17612': 1, '27855': 1, '20516': 1, '31054': 1,
	// 	'93734': 1, '90514': 1, '90670': 1,
	// 	'02550': 1, '04278': 1, '00035': 1, '06050': 1, '01392': 1, '03018': 1, '73440': 1
	// },
	//ignorarMunicipios: {'01392': 1},

	intervaloVerificacao: 10000,

	httpTimeout: 10000,

	/*raizIncludes: '/opt/eleicoes2014/2turno2014/',
	raizBackup: '/opt/eleicoes2014/backup/',
	raizURLMunicipios: '/2turno2014',
	urlBase: 'localhost',
	//urlBase: 'http://apuracao.ig.com.br',
	caminhoHome: '/',*/

	raizIncludes: '/opt/eleicoes2014/1turno2014/',
	raizBackup: '/opt/eleicoes2014/backup/',
	raizURLMunicipios: '/1turno2014',
	urlBase: 'localhost',
	//urlBase: 'http://apuracao.ig.com.br',
	caminhoHome: '/',

	ortc: {
		// appKey: "HbauIb",
		// appKey: 'T3YGds',
		// privateKey: 'KNchnI9kKjqw',
		// privateKey: "yviNT5KxHrZtw", 
		// authToken: "b7060000-f1f9-11e1-aff1-0800200c9a66", 
		authToken: "rts4iv8ma210", 		
		// authToken: "teste123", 		
	    isCluster: true,
	    authRequired: false,
	    // metaData: 'YYY',
	    // authTokenIsPrivate: true,
	    timeToLive: 1400,	
		"AT_CLIENT": "22d6b8b0-1c58-4c89-9704-49b786db61c9", 
		channelPrefix: 'eleicoes2014:',
		authRenew: 30,

		// appKey: 'T3YGds',
		// privateKey: 'KNchnI9kKjqw',

	   appKey: "raGGuI",
	    privateKey: "UYWf18WlaR5o", //PRIVATE KEY
	    // 'AT_SERVER': "rts4iv8ma21", //TOKEN
	    // 'AT_CLIENT': "rwvx3g414s", //TOKEN

		//"CLUSTER_URL": "http://prd2-balancer.realtime.livehtml.net/server/2.0/",
		clusterUrl: "http://ortc-prd.realtime.co/server/2.1"
	}
}

exports.config = config