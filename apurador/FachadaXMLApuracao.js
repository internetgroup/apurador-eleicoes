var http = require('http'),
	request = require('request'),
	JSZip = require('node-zip'),
	BufferJoiner = require('bufferjoiner'),
	logger = require('winston');

var PATH_DIVULGACAO = 'divulgacao',
	PATH_CONFIG = 'config',
	PATH_DISTRIBUICAO = 'distribuicao',
	PATH_COMUM = 'comum';

/*
 * Essa classe prove metodos que facilitam o acesso aos XMLs servidos pelo host do TSE - ou seja, abstrai a 
 * nomenclatura de URLs dos diversos tipo de XML e a descompressao dos arquivos ZIP que guardam os XMLs
 */

function FachadaXMLApuracao (config) {
	this._config = config;
}

FachadaXMLApuracao.cargo = { 
	// cccc - código do cargo
	PRESIDENTE: '0001',
	GOVERNADOR: '0003',
	SENADOR: '0005',
	DEPUTADO_FEDERAL: '0006',
	DEPUTADO_ESTADUAL: '0007',
	DEPUTADO_DISTRITAL: '0008',
	PREFEITO: '0011', 
	VEREADOR: '0013'
}

FachadaXMLApuracao.prototype.obterConfigMunicipios = function (uf, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoConfigMunicipios(uf), onsucess, onfailure);
}

// @abrangencia: uma sigla de UF ou 'br'
FachadaXMLApuracao.prototype.obterIndice = function (abrangencia, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoIndice(abrangencia), onsucess, onfailure);
}

// @abrangencia: uma sigla de UF ou 'br'
FachadaXMLApuracao.prototype.obterVariavelAcompanhamentoAbrangencia = function (abrangencia, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoVariavelAbrangencia(abrangencia), onsucess, onfailure);
}

// @abrangencia: uma sigla de UF ou 'br'
FachadaXMLApuracao.prototype.obterVariavelAbrangencia = function (uf, cargo, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoVariavelUf(uf, cargo), onsucess, onfailure);
}

FachadaXMLApuracao.prototype.obterVariavelMunicipio = function (uf, municipioID, cargo, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoVariavelMunicipio(uf, municipioID, cargo), onsucess, onfailure);
}

FachadaXMLApuracao.prototype.obterVariavelDeMetadados = function (meta, onsucess, onfailure) {
	if (meta.tipoAbrangencia == 'municipio') {
		this.obterVariavelMunicipio(
			meta.idAbrangenciaPai,
			meta.idAbrangencia,
			meta.cargo,
			onsucess,
			onfailure);
	} else {
		if (meta.isAcompanhamento) {
			this.obterVariavelAcompanhamentoAbrangencia(
				meta.idAbrangencia,
				onsucess,
				onfailure);
		} else {
			this.obterVariavelAbrangencia(
				meta.idAbrangencia,
				meta.cargo,
				onsucess,
				onfailure);
		}
	}
}

// @abrangencia: uma sigla de UF ou 'br'
FachadaXMLApuracao.prototype.obterFixoAcompanhamentoAbrangencia = function (abrangencia, versao, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoFixoAcompanhamentoAbrangencia(abrangencia, versao), onsucess, onfailure);
}

// @abrangencia: uma sigla de UF ou 'br'
FachadaXMLApuracao.prototype.obterFixoAbrangencia = function (abrangencia, cargo, versao, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoFixoAbrangencia(abrangencia, cargo, versao), onsucess, onfailure);
}

FachadaXMLApuracao.prototype.obterFixoMunicipio = function (uf, municipioID, cargo, versao, onsucess, onfailure) {
	this._obterXMLdaURL(this._montarCaminhoFixoMunicipio(uf, municipioID, cargo, versao), onsucess, onfailure);
}

FachadaXMLApuracao.prototype.obterFixoDeMetadados = function (meta, onsucess, onfailure) {
	if (meta.tipoAbrangencia == 'municipio') {
		this.obterFixoMunicipio(
			meta.idAbrangenciaPai,
			meta.idAbrangencia,
			meta.cargo,
			meta.versao,
			onsucess,
			onfailure);
	} else {
		if (meta.isAcompanhamento) {
			this.obterFixoAcompanhamentoAbrangencia(
				meta.idAbrangencia,
				meta.versao,
				onsucess,
				onfailure);			
		} else {
			this.obterFixoAbrangencia(
				meta.idAbrangencia,
				meta.cargo,
				meta.versao,
				onsucess,
				onfailure);
		}
	}
}

// @abrangencia: uma sigla de UF ou 'br'
FachadaXMLApuracao.prototype.obterZipIndice = function (abrangencia, onsucess, onfailure) {
	this._obterDaURL(this._montarCaminhoIndice(abrangencia), onsucess, onfailure);
}

FachadaXMLApuracao.prototype.obterZipDentroAbrangencia = function (uf, nomeArquivo, onsucess, onfailure) {
	this._obterDaURL(this._montarCaminhoRaizUF(uf) + '/' + nomeArquivo + '.zip', onsucess, onfailure);
}

FachadaXMLApuracao.prototype._obterDaURL = function (path, onsucess, onfailure) {
	var url = 'http://' + this._config.host + ':' + this._config.porta + path; 
	
	logger.info('Retrieving from ' + url);

	var joiner = new BufferJoiner(),
		options = {
			url: url,
			method: 'GET',
			encoding: null
		};

	if (this._config.proxy)
		options.proxy = this._config.proxy;

	var timeout, aborted = false;
	var self = this;
	timeout = setTimeout(function () {
		aborted = true;
		var msg = 'Timeout for request ' + url + ' - making a new request for it';
		logger.error(msg);
		self._obterDaURL(path, onsucess, onfailure);
	}, this._config.httpTimeout)

	var req = request(options, function (error, res, data) {
			if (aborted) return;	// Caso a requisicao comece a responder depois que ja tenhamos considerado timeout

			if (error) {
				clearTimeout(timeout);
				var msg = error + ' for ' + url;
				logger.error(msg);
				onfailure(msg);
				return;
			}

			if (res.statusCode != 200) {
				clearTimeout(timeout);
				var msg = 'Status ' + res.statusCode + ' for ' + url;
				logger.error(msg);
				onfailure(msg);
				return;
			}

			clearTimeout(timeout);
			onsucess(data, path);
		});
}

FachadaXMLApuracao.prototype._obterXMLdaURL = function (caminhoZip, onsucess, onfailure) {
	var nomeZip = caminhoZip.substring(caminhoZip.lastIndexOf('/') + 1),
		nomeXML = nomeZip.substring(0, nomeZip.length - 4) + '.xml';

	this._obterDaURL(caminhoZip, function (zipData) {
			logger.info('Opening ' + nomeZip);

			try {
				var zip = new JSZip(zipData.toString('base64'), { base64: true });

			} catch (e) {
				var msg = '"' + e.message + '" opening ' + nomeZip + '. Corrupted file?';
				logger.error(msg);
				onfailure(msg);
				return;
			}

			if (!zip.files[nomeXML]) {
				var msg = 'Entry ' + nomeXML + ' was not found in ' + nomeZip;
				logger.error(msg);	
				onfailure(msg)	
				return;			
			}

			logger.info('Extracting ' + nomeXML + ' from ' + nomeZip);

			var contents;
			try {
				contents = zip.file(nomeXML).asBinary();

			} catch (e) {
				var msg = '"' + e.message + '" extracting ' + nomeXML + ' from ' + nomeZip + '. Corrupted file?';
				logger.error(msg);
				onfailure(msg);
				return;
			}

			onsucess(contents, nomeXML);

		}, onfailure);
}

/*
* Métodos das eleições de 2012
*/
FachadaXMLApuracao.prototype._montarCaminhoConfigMunicipios = function (uf) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_CONFIG + '/' + uf + '-e' + c.eleica + '-c002.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoIndice = function (abrangencia) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + abrangencia + '/' + abrangencia + '-e' + c.eleica + '-i.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoVariavelMunicipio = function (uf, municipioID, cargo) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + uf + '/' + uf + municipioID + '-' + cargo + '-e' + c.eleica + '-v.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoVariavelUf = function (uf, cargo) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + uf + '/' + uf + '-' + cargo + '-e' + c.eleica + '-v.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoFixoMunicipio = function (uf, municipioID, cargo, versao) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + uf + '/' + 
		uf + municipioID + '-' + cargo + '-e' + c.eleica + '-f' + versao + '.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoVariavelAbrangencia = function (abrangencia) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + abrangencia + '/' + abrangencia + '-0000-e' + c.eleica + '-v.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoFixoAcompanhamentoAbrangencia = function (abrangencia, versao) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + abrangencia + '/' + 
		abrangencia + '-0000-e' + c.eleica + '-f' + versao + '.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoFixoAbrangencia = function (abrangencia, cargo, versao) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + abrangencia + '/' + 
		abrangencia + '-' + cargo + '-e' + c.eleica + '-f' + versao + '.zip'
}

FachadaXMLApuracao.prototype._montarCaminhoRaizUF = function (uf) {
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' + c.eleicaoID + '/' + 
		PATH_DISTRIBUICAO + '/' + uf;
}

/**
* Novos métodos para eleição de 2014
* Métodos para eleição de presidente, senadores, deputados
* @author tcoelho@igcorp.com.br
* Referências: as documentações estão todas localizadas nesse link
* http://www.tse.jus.br/eleicoes/eleicoes-2014/parceria-divulgacao-resultados-2014
* http://www.justicaeleitoral.jus.br/arquivos/tse-instrucoes-parceiros-divulgacao-eleicoes-2014-versao-1-0
*/

/**
* Método para montar url do certificado digital
* @return path <String> localização do file xml 
*/
FachadaXMLApuracao.prototype._montarCaminhoCertificadoDigital = function(){
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' 
			+ c.eleicaoID + '/' + PATH_CONFIG + '/a' + c.eleica + '-a.cer';
},
/**
* Método para busca do arquivo fixo br
* @param cccc <String> código númerico de 4 digitos do cargo específico
* @param versao
* @return path <String> 
*/
FachadaXMLApuracao.prototype._montarCaminhoFixoBr = function(cccc, versao){
	var c = this._config;
	var a = (abrangencia) ? abrangencia : 'br';
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' 
			+ c.eleicaoID + '/' + PATH_CONFIG + '/' +  PATH_DISTRIBUICAO + '/br' 
			+ cccc + '-e' + c.eleica + '-f' + versao + '.zip';
}, 
/**
* Método para montar url do arquivo fixo de acompanhamento br
* @param versao
* @return path <String>
*/
FachadaXMLApuracao.prototype._montarCaminhoFixoAcompanhamentoBr = function(versao){
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' 
			+ c.eleicaoID + '/' + PATH_CONFIG + '/' +  PATH_DISTRIBUICAO 
			+ '/br-0000-e' + c.eleica + '-f' + versao + '.zip';
},
/**
*
* @param cccc <String> código númerico de 4 digitos do cargo específico
* @return path <String>
*/
FachadaXMLApuracao.prototype._montarCaminhoVariavelBr = function(cccc){
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' 
			+ c.eleicaoID + '/' + PATH_CONFIG + '/' +  PATH_DISTRIBUICAO 
			+ '/br-' + cccc + '-e' + c.eleica + '-v.zip';
},
/**
*
* @return path <String>
*/
FachadaXMLApuracao.prototype._montarCaminhoVariavelAcompanhamentoBr = function(){
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' 
			+ c.eleicaoID + '/' + PATH_CONFIG + '/' +  PATH_DISTRIBUICAO 
			+ '/br-0000-e' + c.eleica + '-v.zip';
},
/**
* 
* @return path <String>
*/
FachadaXMLApuracao.prototype._montarCaminhoIndiceBr = function(){
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' 
			+ c.eleicaoID + '/' + PATH_CONFIG + '/' +  PATH_DISTRIBUICAO 
			+ '/br-e' + c.eleica + '-i.zip';
},
/**
* Método para montar a url do arquivo de eleitos br
* @return path <String>
*/
FachadaXMLApuracao.prototype._montarCaminhoEleitosBr = function(){
	var c = this._config;
	return '/' + c.ano + '/' + PATH_DIVULGACAO + '/' + c.ambiente + '/' 
			+ c.eleicaoID + '/' + PATH_CONFIG + '/' +  PATH_DISTRIBUICAO 
			+ '/br-e' + c.eleica + '-e.zip';
},
/**
* Método para montar a url do arquivo de eleições
* @return path <String>
*/
FachadaXMLApuracao.prototype._montarCaminhoComumConfig = function(){
	var c = this._config;
	return '/' + PATH_COMUM + '/' + PATH_DIVULGACAO + '/' + PATH_DISTRIBUICAO 
			+ '/' + PATH_CONFIG + '/c001.zip';
}


exports.FachadaXMLApuracao = FachadaXMLApuracao;
