function DustTemplate (templateName) {
	this.templateName = templateName;
}

DustTemplate.prototype.toString = function () {
	return '[name=' + this.templateName + ']';
}

DustTemplate.prototype.getContentType = function () {
	return 'text/plain';
}

module.exports = DustTemplate;