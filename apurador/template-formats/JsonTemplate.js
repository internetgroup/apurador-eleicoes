function JsonTemplate (jsonpCallbackName) {
	this.jsonpCallbackName = jsonpCallbackName;
}

JsonTemplate.prototype.toString = function () {
	return '[name=' + this.jsonpCallbackName + ']';
}

JsonTemplate.prototype.getContentType = function () {
	var isJsonp = (typeof this.jsonpCallbackName != 'undefined')
	return 'application/' + (isJsonp ? 'jsonp' : 'json');
}

module.exports = JsonTemplate;