var mongo = require("mongodb"),
	logger = require('winston');

var BSON = mongo.BSONPure;

/*
 * Camada de abstracao de persistencia que lida com documentos em uma base de dados em um servidor MongoDB
 */

function MongoPersistence (config) {
	this._config = config;
	this._db = null;
}

MongoPersistence.prototype.init = function (onsucess, onfailure) {
	this._conectarMongo(onsucess, onfailure);
}

MongoPersistence.prototype._conectarMongo = function (onsucess, onfailure) {
	var db = null,
		cfg = this._config,
		self = this;	

	if (cfg.mongo.servidores) {
		logger.info('Connecting to mongo server at replica set ' + cfg.mongo.replica);

		var servers = [];
		for (var i = 0; i < cfg.mongo.servidores.length; i++) {
			servers.push(new mongo.Server(
					cfg.mongo.servidores[i].host,
					cfg.mongo.servidores[i].port, 
					{ auto_reconnect: true }));
		}
		
		db = new mongo.Db(
				cfg.mongo.db, 
				new mongo.ReplSetServers(servers, 
						{ rs_name: cfg.mongo.replica }));
		
	} else {
		logger.info('Connecting to mongo server at ' + cfg.mongo.host + ':' + cfg.mongo.port);

		db = new mongo.Db(
				cfg.mongo.db, 
				new mongo.Server(
						cfg.mongo.host, 
						cfg.mongo.port, 
						{}),
				{});
	}

	db.open(function(err, p_client) {
		if (err) {
			var msg = 'Could not connect to mongo: ' + err;
			logger.error(msg)
			onfailure(msg);

		} else {
			self._db = db;
			logger.info('Connected to mongo server')
			onsucess();
		}
    });	
}

MongoPersistence.prototype.recuperarColecao = function(colecao, onsucess, onfailure) {
	this._db.collection(colecao, function(err, collection) {
		if (err) {
			var msg = 'Could not get access to collection ' + colecao + ': ' + err;
			logger.error(msg)
			onfailure(msg);
		}
		onsucess(collection);
	});
}

MongoPersistence.prototype.findOne = function (colecao, criterio, onsucess, onfailure) {
	this.recuperarColecao(colecao, function (collection) {
		collection.findOne(criterio, {}, function (err, item) {
			if (err) {
				var msg = 'Could not access object in collection ' + colecao + ' by criteria ' + criterio +
						': ' + err;
				logger.error(msg);
				onfailure(msg);
			}
			onsucess(item)
		})
	}, onfailure)
}

MongoPersistence.prototype.save = function (colecao, doc, onsucess, onfailure) {
	this.recuperarColecao(colecao, function (collection) {
		collection.save(doc, function (err) {
			if (err) {
				var msg = 'Could not insert object in collection ' + colecao + ': ' + err;
				logger.error(msg);
				onfailure(msg);
			}
			onsucess()
		})
	}, onfailure)
}

MongoPersistence.prototype.dropCollection = function (colecao, cb) {
	logger.info('Dropping collection', colecao);
	this._db.dropCollection(colecao, function (err) {
		if (err) {
			var msg = 'Could not drop collection ' + colecao + ': ' + err.stack || err;
			logger.error(msg);
		}		
		cb(err);
	})
}

exports.MongoPersistence = MongoPersistence;
