var utils = require('./utils.js').utils,
    logger = require('winston');

function RenderizadorPrefeitosHome (regras) {
	this._regras = regras;
}

RenderizadorPrefeitosHome.prototype.aplicavel = function (dadosFixos) {
	return dadosFixos.cargo == '0011' && dadosFixos.municipio.capital == 'S';
}

RenderizadorPrefeitosHome.prototype.renderizar = function (arquivoFixo, arquivoVariavel) {
    // console.log('boxPrefeitoHome');
    this._result = null;

    var candidatos = arquivoFixo.candidatos,
        partidos = arquivoFixo.partidos,
        candidatosVar = arquivoVariavel.candidatos,
        porcentagem = ((arquivoVariavel.stats.eleitoradoApurado*100) / arquivoFixo.municipio.eleitorado).toFixed(2),
        porcentagem_brancos_nulos = (parseFloat(arquivoVariavel.stats.votosEmBranco) / arquivoFixo.municipio.eleitorado * 100).toFixed(2)
        realtime = {}

    var eleito = true;

    // Verifica se a eleicao já foi definida e se terá 2 turno
    var eleicaoDefinida = false,
        segundoTurno = false,
        countEleitos = 0;

    if(arquivoVariavel.matematicamenteDefinido && arquivoVariavel.matematicamenteDefinido == 'E'){
            //Realtime
            realtime_eleito = true
            realtime = {
                cidade : arquivoFixo.municipio.nome,
                uf: arquivoFixo.municipio.uf,
                percentual_apurado : porcentagem,
                matematicamenteDefinido : arquivoVariavel.matematicamenteDefinido,
                segundo_turno : false,
                candidatos : [
                  {
                    numero : candidatos[0].numero,
                    nome : candidatos[0].nomeUrna,
                    partido : partidos[candidatos[0].partido].sigla,
                    votos : candidatosVar[candidatos[0].numero].totalVotos
                  }
                ]
            }

            //var porcentagem = ((parseFloat(candidatosVar[candidatos[0].numero].totalVotos) / parseFloat(arquivoVariavel.stats.votosValidos)) * 100).toFixed(2);

            var html = '<div id="uf-' + arquivoFixo.municipio.uf + '" class="apuracao-prefeito uf-' + arquivoFixo.municipio.uf + ' fl">\
              <header>\
                <h4 class="nome-cidade">' + arquivoFixo.municipio.nome + '</h4>\
                <div class="status-apuracao">\
                  <span class="porcentagem">' + porcentagem + '%</span>\
                  <small>de votos<br />apurados</small><br />\
                </div>\
              </header>\
              <div class="content-apuracao">\
                <img src="http://i0.ig.com/ultimosegundo/eleicoes/2012/fotos/'+ utils.normalizarNome(arquivoFixo.municipio.nome) +'-'+ partidos[candidatos[0].partido].sigla.toLowerCase() +'.jpg" width="62" height="83" class="foto-candidato"/>\
                <div class="cand-eleito-info">\
                    <p class="nome-do-eleito"><span class="nome-candidato">'+ candidatos[0].nomeUrna + '</span> <small>('+ partidos[candidatos[0].partido].sigla + ')</small></p>\
                    <p class="num-votos-eleito"><span class="numero-votos">'+ utils.formatNumero(candidatosVar[candidatos[0].numero].totalVotos) + '</span> <small>votos apurados</small></p>\
                    <span class="selo-eleito">Eleito</span>\
                </div>\
              </div>\
              <a href="/1turno2012/'+ arquivoFixo.municipio.uf +'/'+ utils.normalizarNome(arquivoFixo.municipio.nome) +'" class="apuracao-completa">Ver apura&ccedil;&atilde;o completa</a>\
            </div>';

    }else{
             realtime = {
                cidade : arquivoFixo.municipio.nome,
                uf: arquivoFixo.municipio.uf,
                percentual_apurado : porcentagem,
                segundoTurno: false,
                matematicamenteDefinido: false,
                brancos_e_nulos : porcentagem_brancos_nulos,
                candidatos : []
            }
            var html = '<div id="uf-' + arquivoFixo.municipio.uf + '" class="apuracao-prefeito uf-' + arquivoFixo.municipio.uf + ' fl">\
              <header>\
                <h4 class="nome-cidade">' + arquivoFixo.municipio.nome + '</h4>\
                <div class="status-apuracao">\
                  <span class="porcentagem">' + porcentagem + '%</span>\
                  <small>de votos<br>apurados</small><br>\
                </div>\
              </header>\
              <div class="LIE21506 candidatos nomgr">';


            var segundoTurno;
            if(arquivoVariavel.matematicamenteDefinido && arquivoVariavel.matematicamenteDefinido == 'S'){
                segundoTurno = true;
                realtime.segundoTurno = true;
                realtime.matematicamenteDefinido = arquivoVariavel.matematicamenteDefinido
            }

            for (var i=0; i<candidatos.length; i++) {
                var porcentagem_candidato = ((parseFloat(candidatosVar[candidatos[i].numero].totalVotos) / parseFloat(arquivoVariavel.stats.votosValidos)) * 100).toFixed(2);

                porcentagem_candidato = isNaN(porcentagem_candidato) ? "0.00" : porcentagem_candidato

                realtime['candidatos'].push({
                    posicao : "-",
                    nome : candidatos[i].nomeUrna,
                    numero : candidatos[i].numero,
                    partido : partidos[candidatos[i].partido].sigla,
                    porcentagem : porcentagem_candidato
                });

                var classeSeg = (segundoTurno && i < 2) ? ' segundo-turno' : '';

                html += '<div class="candidato '+ partidos[candidatos[i].partido].sigla.toLowerCase() + classeSeg + '">\
                      <span class="nome-candidato"><strong>'+ candidatos[i].nomeUrna + '</strong><span class="nome-partido">('+ partidos[candidatos[i].partido].sigla + ')</span></span>';

                        if(segundoTurno && i < 2){
                            html += '<div class="percentual segundo-turno">\
                                <big>'+ porcentagem_candidato + '%</big><small> - 2&ordm; TURNO</small>\
                              </div>';
                        }else{
                            html += '<div class="percentual">\
                                <span class="bar-100"><span class="bar-perc" style="width:'+ porcentagem_candidato + '%;"></span></span><big>'+ porcentagem_candidato + '%</big>\
                              </div>';
                        }
                html +='<div class="clear">\
                      </div>\
                    </div>';

                if(i >= 2){
                    break;
                }
            }
            html += '<a href="/1turno2012/'+ arquivoFixo.municipio.uf +'/'+ utils.normalizarNome(arquivoFixo.municipio.nome) +'" class="apuracao-completa">Ver apura&ccedil;&atilde;o completa</a>\
                </div></div>';
    }

    this._result = {json: 'apuracao_home_callback(' + JSON.stringify(realtime) + ')', html: html};
    return true;

}

RenderizadorPrefeitosHome.prototype.getDescricao = function () {
	return 'Box de prefeitos'
}

RenderizadorPrefeitosHome.prototype.getURLPaginaContainer = function (dadosFixos) {
  return [ { url: this._regras.obterURLHomeEleicoes(dadosFixos), seletor: '#uf-' + dadosFixos.municipio.uf } ];
}

RenderizadorPrefeitosHome.prototype.obterCaminhoGravacao = function (dadosFixos, formato) {
    if (formato == 'html')
        return this._regras.obterCaminhoIncludePrefeitoHome(dadosFixos);

    if (formato == 'json')
        return this._regras.obterCaminhoJsonPrefeitoHome(dadosFixos);

    msg = 'No render result for format ' + formato;
    logger.error(msg);
    throw new Error(msg);
}

exports.RenderizadorPrefeitosHome = RenderizadorPrefeitosHome;