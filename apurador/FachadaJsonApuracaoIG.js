var FachadaXMLApuracao = require ('./FachadaXMLApuracao.js').FachadaXMLApuracao,
	ParseStrategyFixo = require('./parsers/ParseStrategyFixo'),
	ParseStrategyIndice = require('./parsers/ParseStrategyIndice'),
	ParseStrategyVariavel = require('./parsers/ParseStrategyVariavel'),
	XMLParser = require('./parsers/XMLParser'),
    Classificador = require('./ClassificadorArquivos'),
	logger = require('winston');

/*
 * Essa classe converte o conteudo dos XMLs vindos do TSE em JSON.
 * Durante a conversao, essa classe eh ciente de quais informacoes sao necessarias para a 
 * renderizacao das paginas do iG e quais sao descartaveis.
 * O JSON final, portanto, eh muito mais enxuto que o XML original.
 */

function FachadaJsonApuracaoIG (config) {
	this._fachadaXML = new FachadaXMLApuracao(config);
}

FachadaJsonApuracaoIG.prototype.obterFixo = function (nomeArquivo, onsucess, onfailure) {
	var meta = Classificador.classificar(nomeArquivo);

	var self = this;
	this._fachadaXML.obterFixoDeMetadados(meta, 
			function (xml, nomeXML) {
				self.parseXML(xml, nomeXML, ParseStrategyFixo, onsucess, onfailure);
			}, 
			onfailure)
}

FachadaJsonApuracaoIG.prototype.obterVariavel = function (nomeArquivo, onsucess, onfailure) {
	var meta = Classificador.classificar(nomeArquivo);

	var self = this;
	this._fachadaXML.obterVariavelDeMetadados(meta, 
			function (xml, nomeXML) {
				self.parseXML(xml, nomeXML, ParseStrategyVariavel, onsucess, onfailure);
			}, 
			onfailure)
}

FachadaJsonApuracaoIG.prototype.obterIndice = function (abrangencia, onsucess, onfailure) {
	var self = this;
	this._fachadaXML.obterIndice(abrangencia, 
			function (xml, nomeXML) {
				self.parseXML(xml, nomeXML, ParseStrategyIndice, onsucess, onfailure);
			}, 
			onfailure)
}

FachadaJsonApuracaoIG.prototype.parseXML = function (xml, nomeXML, strategy, onsucess, onfailure) {
	logger.info('Parsing file', nomeXML);
	XMLParser.parse(xml, strategy, 
		function (result) {
			// console.log(result);
			onsucess(result);
		}, 
		function (err) {
			logger.error('Failed to parse file', nomeXML, 
				err.stack || err)
			onfailure(err);			
		});
}

exports.FachadaJsonApuracaoIG = FachadaJsonApuracaoIG;
