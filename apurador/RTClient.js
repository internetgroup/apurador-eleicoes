var logger = require('winston');
var OrtcNodeclient 	= require('ibtrealtimesjnode').IbtRealTimeSJNode;
var async = require('async');

/**
 * @class - manages the communication with an ortc's client instance
 * @param {Object} config [configurarion parameters related to the app]
 */
function RTClient( config ){
	this.client = new OrtcNodeclient();
	this.config = config;
}

/**
 * @interface should be implemented to listen for connections
 * @return {void}
 */
RTClient.prototype.onConnected = function(){

	logger.warn('You should implement RTClient\'s interface method onConnected to listen for connections!');
	return undefined;
};

/**
 * @interface should be implemented to listen for disconnections
 * @return {void}
 */
RTClient.prototype.onDisconnected = function(){

	logger.warn('You should implement RTClient\'s interface method onDisconnected to listen for disconnections!');
	return undefined;
};

/**
 * @interface should be implemented to listen for reconnections
 * @return {void}
 */
RTClient.prototype.onReconnected = function(){

	logger.warn('You should implement RTClient\'s interface method onReconnected to listen for reconnections!');
	return undefined;
};

/**
 * @interface should be implemented to listen for exceptions
 * @return {void}
 */
RTClient.prototype.onException = function(){

	logger.warn('You should implement RTClient\'s interface method onException to listen for exceptions!');
	return undefined;
};

/**
 * @interface should be implemented to listen for channels' subscribes
 * @return {void}
 */
RTClient.prototype.onSubscribed = function(){

	logger.warn('You should implement RTClient\'s interface method onSubscribed to listen for subscribes!');
	return undefined;
};

/**
 * @interface should be implemented to listen for channels' unsubscribes
 * @return {void}
 */
RTClient.prototype.onUnsubscribed = function(){

	logger.warn('You should implement RTClient\'s interface method onUnsubscribed to listen for unsubscribes!');
	return undefined;
};

/**
 * @interface should be implemented to listen for reconnections
 * @return {void}
 */
RTClient.prototype.onReconnecting = function(){

	logger.warn('You should implement RTClient\'s interface method onReconnecting to listen for reconnection!');
	return undefined;
};

/**
 * Checks if a client is subscribed to a list of channels
 * @param  {Array<String>}  channels [list of channels]
 * @return {Boolean}          [true if client is subscribed to all channels, and false, otherwise]
 */
RTClient.prototype.isSubscribedToChannels = function(channels){
	var self = this;
	var lenChannels = channels.length;

	for ( var i = 0 ; i < lenChannels ; i++ ) {
		var channel = channels[i];
		var isSubscribed = self.isSubscribedToChannel( channel );

		if ( !isSubscribed ) return false;
	}

	return true;
};

/**
 * Checks if a client is subscribed to a single channel
 * @param  {String}  channel [the channel id]
 * @return {Boolean}          [true if client is subscribed to the channel, and false, otherwise]
 */
RTClient.prototype.isSubscribedToChannel = function( channel ){
	var self = this;
	return self.client.isSubscribed( channel );
};

/**
 * Subscribes the client to the suplied channel
 * @param  {String} channel [the channel id the client wants to subscribe to]
 * @param {Function} callback [when done, notifies]
 * @return {void}
 */
RTClient.prototype.subscribeChannel = function(channel, callback){
	var self = this;
	var subscribeOnReconnected = true;

	logger.info('Subscribing to channel', channel);
	self.client.subscribe( channel, subscribeOnReconnected, function(){
		if ( callback ) callback();
	});
};

/**
 * Subscribes the client to a list of channels
 * @param  {Array<String>} channels [the list of channels the client wants to subscribe to]
 * @return {void}          
 */
RTClient.prototype.subscribeChannels = function(channels){
	var self = this;
	var lenChannels = channels.length;

	logger.info('Subscribing a list of %s channels', lenChannels);

	for ( var i = 0 ; i < lenChannels ; i++ ) {
		var channel = channels[i];

		self.subscribeChannel( channel );
	}	
};

/**
 * Sends a message to the suplied channel
 * @param  {String} channel [the channel id the message should be sent to]
 * @param  {Object} message [the message object to send to the provided channel]
 * @return {void}         
 */
RTClient.prototype.sendMessage = function( channel, message ){
	var self = this;

	logger.info('Sending to channel: %s new message: %s', channel, message);
	self.client.send( channel, message );
};

/**
 * Sets up logging of some available ortc's callbacks
 */
RTClient.prototype.setUpCallbacks = function(){
	// TODO
	// rename method name from setUpCallbacks to setUpEvents
	var self = this;

	self.client.onConnected = function(ortc){
		logger.info('Connected to cluster url:', self.config.clusterUrl);
		self.onConnected();
	};

	self.client.onDisconnected = function(ortc){
		logger.info('Disconnected from cluster url:', self.config.clusterUrl);
		self.onDisconnected();
	};

	self.client.onReconnecting = function(ortc){
		logger.info('Reconnecting to cluster url:', self.config.clusterUrl);
		self.onReconnecting();
	};

	self.client.onReconnected = function(ortc){
		logger.info('Reconnected to cluster url:', self.config.clusterUrl);
		self.onReconnected();
	};

	self.client.onSubscribed = function( ortc, channel, message ){
		logger.info('Successfully subscribed to channel:', channel);
		self.onSubscribed();
	};

	self.client.onException = function(ortc, error){
		logger.error('Exception:', error);
		self.onException( ortc, error );
	};
};

/**
 * Connects the client to the server
 * @return {void} 
 */
RTClient.prototype.connect = function(){
	var self = this;
	self.client.connect(self.config.appKey, self.config.authToken);
};

/**
 * Authenticate the client to the suplied channel to enable posting messages
 * @param  {String}   channel  [the channel id to authenticate the client to]
 * @param  {Function} callback [error, authenticated?]
 * @return {void}            
 */
RTClient.prototype.authenticateChannel = function( channel, callback ){
	var self = this;
	var permissions = JSON.parse('{"'+channel+'" : "w"}');

	logger.info('Authenticating channel:', channel);

	// settings for post permissions
	self.client.saveAuthentication(
		self.config.clusterUrl, self.config.isCluster, 
		self.config.authToken, self.config.authIsPrivate, self.config.appKey, self.config.timeToLive, 
		self.config.privateKey, permissions, 
		function (error, success) {
		    if (error) {
		    	logger.error('Error authenticating channel %s: %s', channel, error);
		    	callback( error, false );
		    } else if (success) {
		        logger.info('Successfully authenticated channel:', channel);
		        callback( false, true );
		    } else {
		        logger.error('Not authenticated channel:', channel);
		        callback( true, false );
		    }
	});
};

/**
 * Authenticate the client to a list of channels to enable posting messages
 * @param  {Array<String>}   channels [a list of channels the client should authenticate to]
 * @param  {Function} callback [error, success]
 * @return {void}
 */
RTClient.prototype.authenticateChannels = function(channels, callback){
	var self = this;

	logger.info('Authenticating a list of %s channels', channels.length);

	async.every( channels, function( channel, channelCallback ){
		self.authenticateChannel( channel, function( error, authenticated ){
			// TODO
			// should pass the 'authenticated' variable to the callback
			// if any authentication fail, the method callback should notify an error
			channelCallback(true);
		});
	}, function( channelsAuthenticated ){
		// TODO
		// Should not only log when not all channels successfully authenticate, but notify in the callback to
		if ( !channelsAuthenticated ) return logger.error('Some channels did not authenticated, you will not be able to subscribe to them. Try again!');
		callback();
	});
};

/**
 * Sets up the client's cluster url, connection metadata and ortc's callbacks
 */
RTClient.prototype.setUp = function(){
	var self = this;

	self.client.setClusterUrl( self.config.clusterUrl );
	// TODO
	// check the usage of metadata. Looks like its not being used correctly
	self.client.setConnectionMetadata( self.config.metaData );
	self.setUpCallbacks();
};

module.exports = RTClient;
