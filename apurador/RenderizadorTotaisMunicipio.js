var utils = require('./utils.js').utils,
    logger = require('winston');

var capitais = {
        "SP" : {nome : "São Paulo", capital : "sao-paulo"},
        "RJ" : {nome : "Rio de Janeiro", capital : "rio-de-janeiro"},
        "MG" : {nome : "Minas Gerais", capital : "belo-horizonte"},
        "ES" : {nome : "Espírito Santo", capital : "vitoria"},
        "RS" : {nome : "Rio Grande do Sul", capital : "porto-alegre"},
        "PR" : {nome : "Paraná", capital : "curitiba"},
        "SC" : {nome : "Santa Catarina", capital : "florianopolis"},
        "BA" : {nome : "Bahia", capital : "salvador"},
        "CE" : {nome : "Ceará", capital : "fortaleza"},
        "PE" : {nome : "Pernambuco", capital : "recife"},
        "MA" : {nome : "Maranhão", capital : "sao-luis"},
        "PI" : {nome : "Piauí", capital : "teresina"},
        "RN" : {nome : "Rio Grande do Norte", capital : "natal"},
        "AL" : {nome : "Alagoas", capital : "maceio"},
        "PB" : {nome : "Paraíba", capital : "joao-pessoa"},
        "SE" : {nome : "Sergipe", capital : "aracaju"},
        "GO" : {nome : "Goiás", capital : "goiania"},
        "MS" : {nome : "Mato Grosso do Sul", capital : "campo-grande"},
        "MT" : {nome : "Mato Grosso", capital : "cuiaba"},
        "AM" : {nome : "Amazonas", capital : "manaus"},
        "PA" : {nome : "Pará", capital : "belem"},
        "RO" : {nome : "Rondônia", capital : "porto-velho"},
        "AP" : {nome : "Amapá", capital : "macapa"},
        "AC" : {nome : "Acre", capital : "rio-branco"},
        "RR" : {nome : "Roraima", capital : "boa-vista"},
        "TO" : {nome : "Tocantins", capital : "palmas"}
    };

function RenderizadorTotaisMunicipio (regras) {
	this._regras = regras;
}

RenderizadorTotaisMunicipio.prototype.aplicavel = function (dadosFixos) {
	return dadosFixos.cargo == '0011'; // Apenas para o XML de prefeito
}

RenderizadorTotaisMunicipio.prototype.renderizar = function (arquivoFixo, arquivoVariavel) {
    this._result = null;

    estado = capitais[arquivoFixo.municipio.uf.toUpperCase()].nome
    cidade = arquivoFixo.municipio.nome
    total_votos = arquivoFixo.municipio.eleitorado
    percentual_apurado =  arquivoVariavel.stats.eleitoradoApurado / total_votos * 100 
    percentual_apurado = percentual_apurado.toFixed(2)
    votos_brancos = parseFloat(arquivoVariavel.stats.votosEmBranco)
    votos_nulos = parseFloat(arquivoVariavel.stats.votosNulos)
    votos_brancos_nulos =  votos_brancos +  votos_nulos
    votos_brancos_nulos = votos_brancos_nulos.toFixed(0)
    ultima_atualizacao = arquivoVariavel.stats.dataTotalizacao+' '+arquivoVariavel.stats.horaTotalizacao

    var template = '<div class="prefeito-cidade" id="resumo-cidade">\
            <header>\
              <div class="fl">\
                <small class="estado">'+estado+'</small>\
                <h2 class="nome-cidade">'+cidade+'</h2>\
              </div>\
              <div class="fr">\
                <span class="porcentagem">'+percentual_apurado+'%</span>\
                <small class="apurados">de votos<br>apurados<br></small>\
              </div>\
              <div class="fr">\
                <small class="last-update">&uacute;ltima atualiza&ccedil;&atilde;o <br /> <b>'+ultima_atualizacao+'</b></small>\
              </div>\
            </header>\
            <div class="dados-prefeito">\
              N&uacute;mero de eleitores: <strong>'+utils.formatNumero(total_votos)+'</strong>\
            </div>\
            <div class="dados-prefeito">\
              Brancos e nulos: <strong>'+utils.formatNumero(votos_brancos_nulos)+'</strong>\
            </div>\
            <div class="dados-prefeito">\
              Número de se&ccedil;&otilde;es: <strong>'+utils.formatNumero(arquivoFixo.municipio.secoes)+'</strong>\
            </div>\
            <div class="dados-prefeito">\
              Absten&ccedil;&otilde;es: <strong>'+utils.formatNumero(arquivoVariavel.stats.abstencao)+'</strong>\
            </div>\
            <div class="dados-prefeito">\
              Votos apurados: <strong>'+utils.formatNumero(arquivoVariavel.stats.eleitoradoApurado)+'</strong>\
            </div>\
          </div>\
          <!-- / Resultados prefeitos interna -->'

    realtime = {
        estado : estado,
        cidade : cidade,
        percentual_apurado : percentual_apurado,
        ultima_atualizacao : ultima_atualizacao,
        numero_eleitores : utils.formatNumero(total_votos),
        votos_brancos_nulos : utils.formatNumero(votos_brancos_nulos),
        numero_secoes : utils.formatNumero(arquivoFixo.municipio.secoes),
        abstencoes : utils.formatNumero(arquivoVariavel.stats.abstencao),
        votos_apurados : utils.formatNumero(arquivoVariavel.stats.eleitoradoApurado)
    }

    this._result = {json:JSON.stringify(realtime),html:template}
    return true;
}

RenderizadorTotaisMunicipio.prototype.getDescricao = function () {
	return 'Totais de municipio'
}

RenderizadorTotaisMunicipio.prototype.getURLPaginaContainer = function (dadosFixos) {
  return [ { url: this._regras.obterURLPaginaMunicipio(dadosFixos), seletor: '#resumo-cidade' } ];
}

RenderizadorTotaisMunicipio.prototype.obterCaminhoGravacao = function (dadosFixos, formato) {
    if (formato == 'html')
        return this._regras.obterCaminhoIncludeTotaisMunicipio(dadosFixos);

    if (formato == 'json')
        return this._regras.obterCaminhoJsonTotaisMunicipio(dadosFixos);

    msg = 'No render result for format ' + formato;
    logger.error(msg);
    throw new Error(msg);
}

exports.RenderizadorTotaisMunicipio = RenderizadorTotaisMunicipio;