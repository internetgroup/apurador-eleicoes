#Apurador de eleições

O sistema apurador de eleições é responsável por consumir dados brutos da apuração de uma determinada eleição, processá-los e transformá-los em um formato amigável ao usuário final (geralmente na forma de arquivos HTML ou JSON).

O principal processo do apurador é o daemon `apurador.js`, residente na pasta `apurador`. Uma vez inicializado, este processo consulta periodicamente o servidor do TSE e procura por atualizações de dados durante o progresso da eleição sendo apurada.

Quando é detectada alguma atualização, os dados atualizados são baixados. De acordo com a natureza da informação atualizada (cargo, abrangência ou outros parâmetros) uma sequência específica de processamento será aplicada aos dados. Esta sequência geralmente consiste em inserir os dados em um ou mais templates, gravá-los em disco (com formato HTML ou JSON) e emitir uma notificação de atualização via RealTime para um ou mais canais. Várias etapas deste processamento são customizáveis. 

O projeto também inclui, no diretório `test-server`, um servidor HTTP que emula o servidor do TSE. Durante a customização do sistema para uma futura eleição (criação de templates etc), este servidor pode ser usado como origem dos dados de apuração, a fim de tornar mais fácil esta tarefa de customização. 

Os processos descritos acima são tratados com mais detalhe no decorrer da documentação.

##Download

O projeto pode ser baixado através do clone do repositório:

    git clone https://{nome_de_usuario}@bitbucket.org/internetgroup/apurador-eleicoes.git

##Dependências

O apurador necessita que as seguintes dependências estejam instaladas:

* Node.js (0.10 ou maior)
* NPM (1.3 ou maior)
* MongoDB

As dependências de bibliotecas devem ser instaladas da seguinte forma:

No diretório `{raiz_do_projeto}/apurador`: 

    npm install

No diretório `{raiz_do_projeto}/test-server`:

    npm install

##Configuração do ambiente de desenvolvimento

###Banco de dados

Uma instância do MongoDB deve estar ativa. O seguinte comando de exemplo pode ser usado para iniciar uma instância local:

    mongod --port 27017 --dbpath /Users/rgermano/iG/mongo-apuracao/

###Arquivo de configuração do daemon apurador

O projeto inclui um arquivo de configuração do daemon de apuração (no caminho `apurador/local/apurador-cfg.js`) para que seja possível rodar o sistema localmente. 

Para utilizar localmente o servidor de teste `test-server` como fonte de dados fictícios de apuração, o arquivo de configuração `local/apurador-cfg.js` deve conter a seguinte configuração:

```javascript
host: localhost,
porta: 8080,
ano: '2014',
eleicaoID: '4464',
eleica: '001441',
ambiente: 'oficial'
```

`mongo`: A entrada `mongo` no arquivo de configuração deve apontar para a instância local do MongoDB inicializada no passo anterior:

```javascript
mongo: {
	host: 'localhost',
	port: 27017,
	db: 'eleicoes2014_local' //_dev'
},
```

`abrangencias`: De acordo com a necessidade do desenvolvedor, o apurador pode procurar por atualizações em todas as abrangências (que incluem o Brasil e os estados do Brasil), ou em abrangências específicas. Isto é configurado na entrada `abrangencias`:

```javascript
abrangencias: [
	'br',
	'sp', 'rj', 'mg', 'es', 'df',
	'rs', 'pr', 'sc',
	'ba', 'ce', 'pe', 'ma', 'pi', 'rn', 'al', 'pb', 'se',
	'go', 'ms', 'mt',
	'am', 'pa', 'ro', 'ap', 'ac', 'rr', 'to'
]
```

`ignorarTodosMunicipios`: Para apurar eleições para prefeito e vereador, normalmente é necessário buscar atualizações em cada município do Brasil. Para que o apurador faça essa busca, a entrada `ignorarTodosMunicipios` deve ter o valor `false`. 

Porém, para eleições dos demais cargos (presidente, senador, governador, deputado federal e deputado estadual), geralmente é suficiente buscar por atualizações apenas nos estados do Brasil e na totalização do Brasil. Para este tipo de situação, o sistema pode ignorar completamente os arquivos de atualização de município quando a entrada `ignorarTodosMunicipios` tiver o valor `true`.

`cacheServers`: Depois que o apurador processa atualizações de dados da apuração e gera arquivos estáticos em disco, pode ser desejável que os servidores de cache (Varnish) sejam notificados da atualização, para que removam do cache as entradas referentes a esses arquivos. Para isso, basta acrescentar os hosts dos servidores Varnish neste array. Porém para o ambiente de desenvolvimento, geralmente esse array ficará vazio:

```javascript
cacheServers: [
	//'cms-cache-mid-3.aws.ig.com.br',
	//'cms-cache-mid-4.aws.ig.com.br'
],
```

`baseUrl`: Quando a propriedade `cacheServers` não for vazia, as URLs a serem expiradas dos servidores de cache serão baseadas na propriedade `baseUrl`.

`templateDir`: Um dos passos do ciclo do apurador é inserir os dados atualizados em templates. Essa configuração deve apontar para o caminho (__absoluto!__) onde esses templates podem ser encontrados: Por exemplo, para as eleições de 2014, esses templates foram armazenados na pasta `apurador/2014/templates`, então a configuração de desenvolvimento ficou:

```javascript
templateDir: '/var/www/apurador-eleicoes/apurador/2014/templates',
```

`publishDestination`: Deve apontar para o caminho base absoluto onde o apurador irá gravar os arquivos estáticos resultantes do processamento das atualizações da apuração. De acordo com a customização feita pelo desenvolvedor, o apurador poderá gerar automaticamente estruturas de subdiretórios dentro desse caminho.

`staticPageDestination`: Configuração utilizado pelo utilitário `page-gen` de geração de arquivos HTML estáticos. Mais detalhes no decorrer da documentação.

`intervaloVerificacao`: Intervalo (em milissegundos) em que o apurador deverá procurar por atualizações no servidor de dados da apuração.

`httpTimeout`: Tempo máximo de espera de uma requisição feita ao servidor de dados da apuração antes de considerá-la como falha.

`ortc`: Ao encontrar uma atualização de dados durante a apuração, o apurador pode ser configurado para emitir uma ou mais mensagens via RealTime para notificar a atualização. A entrada `ortc` na configuração define a conexão RealTime a ser usada para este passo:

```javascript
ortc: {
	appKey: 'T3YGds',
	privateKey: 'KNchnI9kKjqw',
	authToken: "rts4iv8ma210", 		
	clusterUrl: "http://ortc-prd.realtime.co/server/2.1"
	isCluster: true,
	authRequired: false,
	timeToLive: 1400,	
	channelPrefix: 'eleicoes2014:'
}
```

__Nota:__ O sistema apurador não é responsável por dar permissões de escrita aos canais e ao usuário descritos na configuração. Tal autenticação é necessária ser feita por outra aplicação. Vale ressaltar que, até o momento, contas de desenvolvedor do RealTime não necessitam de autenticação prévia para que a escrita e leitura nos canais funcione.

##Customização do apurador

A customização do sistema apurador consiste em definir qual sequência de processamento será aplicada a cada tipo de arquivo consumido da fonte de dados apuração (o servidor oficial do TSE ou o servidor de dados fictícios - o `test-server`).

###Classificação dos arquivos obtidos da fonte de dados de apuração

Cada arquivo consumido da fonte de dados é primeiramente classificado para determinar sua natureza. O resultado da classificação de um arquivo é a estrutura de dados `ClassificacaoArquivoVariavel` descrita abaixo:

```javascript
// @class ClassificacaoArquivoVariavel
{
	tipo: null,				// fixo|variavel
	nomeArquivo: 'nome original do arquivo'
	tipoAbrangencia: null,	// br|zz|uf|municipio
	isAcompanhamento: null,	// boolean
	cargo: null,			// 0000
	idAbrangencia: null,	// br|zz|nome da uf|id municipio
	idAbrangenciaPai: null,	// nome da uf quando abrangencia eh municipio
	versao: null			// para arquivos fixos
}
```

Este objeto de classificação será útil posteriormente no momento de definir que processamento será aplicado a determinado arquivo de dados atualizado obtido da fonte de dados da apuração.

O campo `cargo` da classificação contém o código do cargo ao qual o arquivo se refere. Ele pode conter um dos seguintes valores:

```javascript
Presidente = '0001'
Governador = '0003'
Senador = '0005'
Deputado federal = '0006'
Deputado estadual = '0007'
Deputado distrital = '0008'
Prefeito = '0011'
Vereador = '0013'
```

###Preparando o ambiente para a customização da aplicação

Para cada nova eleição, a aplicação deve ser customizada para ser apta a processar os dados de apuração de maneira adequeada. As customizações não são obstrusivas, ou seja, não são feitas alterando o core do sistema de apuração, mas sim feitas em pastas separadas no projeto. O diretório `apuracao/2014`, por exemplo, é um caso de customização para as eleições ocorridas em 2014.

Assim, o primeiro passo para customizar a aplicação para uma futura eleição (digamos, do ano 2016) é replicar  a pasta `apuracao/20XX`, que contém o mínimo necessário para o daemon apurador rodar:

    cp -R apurador/20XX apurador/2016

Agora a pasta `apuracao/2016` deve conter as subpastas `1-turno` e `2-turno`, para acomodar as customizações específicas para cada turno, bem como a pasta `templates`, ainda vazia.

###Includes

Cada arquivo de dados baixado pelo apurador contém uma atualização da situação atual da apuração em uma determinada abrangência (município, estado ou Brasil). O arquivo pode ser também referente a um cargo específico ou simplesmente uma sumarização da abrangência. Todas essas informações são providas pela 
estrutura `ClassificacaoArquivoVariavel` descrita anteriormente.

O funcionamento básico do apurador é que, para cada arquivo de dados consumido, zero ou mais _includes_ serão gerados no diretório de publicação (entrada `publishDestination` no arquivo de configuração). Há dois tipos de includes: `IncludeCMS` e `PaginatedCMSInclude`.

###IncludeCMS

A classe `IncludeCMS` representa um único arquivo no diretório de publicação, gerado a partir de um arquivo de dados de apuração. Seu construtor é:

```javascript
//@constructor
function IncludeCMS(
		scriptLogica,
		template,
		nome,
		canaisRealtime,
		dados
	) {}
```

`scriptLogica`: antes de inserir os dados `dados` no template, pode ser desejável aplicar um pré-processamento aos dados (como, por exemplo, ordenar uma lista). O parâmetro `scriptLogica` deve ser o nome do arquivo de script contendo a lógica de pré-processamento do include. Este arquivo deve exportar uma classe com o método `preProcessData(data)`, que recebe um objeto de dados do include, e deve retornar um objeto contendo os dados processados. Estes dados processados que serão consultados no momento de popular o template. O apurador busca o arquivo de script no mesmo diretório configurado para os templates (entrada `templateDir` no arquivo de configuração). Há vários exemplos  de pré-processadores no diretório `apurador/2014/templates`.

`template`: uma instância que implemente a classe `Template`. Até o momento, duas classes implementam esta interface: `JsonTemplate`, para gerar includes em formato JSON, e `DustTemplate`, para gerar includes em formato texto livre (possivelmente HTML), utilizando a linguagem de template Dust (by Linkedin). Essas duas classes estão detalhadas posteriormente nesta documentação.

`nome`: nome do include para diversos fins como, por exemplo, determinar o nome do arquivo do include em disco, ou para identificar o include que foi atualizado no conteúdo de uma mensagem RealTime. A fim de evitar conflitos, cada include deve ter um nome exclusivo dentro uma customização.

`canaisRealtime`: array de nomes de subcanais que nos quais será postada uma mensagem de atualização, a cada vez que o include for atualizado.

`dados`: objeto contendo os dados para montagem do include. Como já mencionado, eles primeiramente serão pré-processados pela classe exportada no arquivo definido pelo parâmetro `scriptLogica`, e depois, inseridos no template definido pelo parâmetro `template`.

###PaginatedCMSInclude 

A classe `PaginatedCMSInclude` representa um ou mais arquivos  no diretório de publicação, gerados a partir do particionamento dos dados contidos em um arquivo de dados de apuração (utilizado quando há necessidade de paginação dos dados no site). Seu construtor é:

```javascript
//@constructor
function PaginatedCMSInclude(
		scriptLogica,
		template,
		nome,
		canaisRealtime,
		dados,
		splitter
	) {}
```

Para uma explicação dos parâmetros `scriptLogica`, `template`, `nome`, `canaisRealtime` e `dados`, vide o tópico `IncludeCMS` acima.

`splitter`: Uma instância que implemente a interface `ResultSplitter`, que contém apenas um método, o `split(data)`. Este método recebe os dados do include (já pré-processados pela classe definida por `scriptLogica`), e deve retornar um array de objetos de dados, em que cada item representa os dados de uma página deste include. Para cada item deste array, o apurador aplica o template definido pelo parâmetro `template`, gerando um arquivo por página no sistema de arquivos. Existe, até o momento, apenas uma classe que implementa a interface `ResultSplitter`: a `VotoCandidatoSplitter`, localizada no diretório `apurador/common`. Seu construtor é 

```javascript
    new VotoCandidatoSplitter(numItensPorPagina)
```

Seu método `split` divide o array de votos de candidato em páginas de tamanho `numItensPorPagina`.

O splitter `VotoCandidatoSplitter` foi utilizado nas eleições de 2014 para paginar a lista de votos para os cargos de deputado federal e estadual. Assim, por exemplo, para uma lista de 1000 candidatos e com o splitter instanciado por `new VotoCandidatoSplitter(10)`, o apurador gera 100 páginas (arquivos em disco) contendo 10 candidatos em cada uma. Note que a lista já foi ordenada previamente (do candidato mais votado para o menos votado) pelo pré-processador (parâmetro `scriptLogica`).

###Mapeamento do processamento dos arquivos de dados

Para cada arquivo de atualização de dados de apuração baixado pelo sistema, o apurador irá executar os seguintes passos:

1. Consultar na classe de customização `CMSIncludeFactory` quais são os includes a serem produzidos a partir do arquivo de dados dado (esta classe será tratada em detalhes mais abaixo);
2. Gerar os includes e gravá-los no sistema de arquivos (no diretório configurado na entrada `publishDestination` no arquivo de configuração);
3. Notificar os servidores de cache sobre a nova versão dos arquivos de include regerados nesse ciclo;
4. Enviar mensagens via RealTime notificando sobre a atualização dos includes regerados neste ciclo.

###A classe CMSIncludeFactory

O arquivo que centraliza as definições de uma customização é o `CMSIncludeFactory.js`, que contém a classe `CMSIncludeFactory`, cujo único método é o `makeIncludesForData`. Note que cada customização deve ter o seu próprio arquivo `CMSIncludeFactory.js`, portanto cada um dos diretórios `1-turno` e `2-turno` tem a sua própria classe `CMSIncludeFactory`, com as customizações particulares para cada um dos turnos.

O método `makeIncludesForData(metadata, dadosFixos, dadosVariaveis)`, é chamado pelo apurador a cada arquivo de dados variáveis recebido pela aplicação, e deve retornar um array de includes a serem gerados para esta atualização. O método recebe três argumentos:

`metadata`: instância da classe `ClassificacaoArquivoVariavel`, com a classificação deste arquivo de dados. A partir deste objeto pode-se saber a abrângência e cargo aos quais o arquivo de se refere, entre outras informações úteis, para ajudar a determinar que includes serão gerados a partir deste arquivo.

`dadosFixos`: ao baixar um arquivo de dados váriáveis contendo uma atualização da apuração, o apurador automaticamente resgata de seu cache o objeto de dados fixos referenciados pelos dados varíaveis, e fornece ao método através deste argumento.

`dadosVariaveis`: objeto contendo os dados variáveis baixados pelo apurador.

__Nota__: Ambos os arquivos de dados fixos e dados variáveis são fornecidos pelo servidor do TSE no formato XML. Para facilitar a manipulação desses dados na aplicação, esses arquivos são transformados em objetos nativos de javascript (antes de serem passados ao método `makeIncludesForData`), em uma estrutura de propriedades muito semelhante à estrutura de tags dos arquivos XML. Para mais detalhes sobre como esses arquivos XMLs são transformados em JSONs, vide as classes de parseamento localizadas na pasta `apurador/parsers` do projeto.

As implementações das classes `CMSIncludeFactory` nas pastas `apurador/2014/1-turno` e `apurador/2014/2-turno` contém vários exemplos de tratamento dos metadados e determinação dos includes a serem gerados para diversos tipos de dados variáveis.

