var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');
var winston = logger = require('winston')
var app = express();
var config = require('../apurador/' + process.argv[2] + '/apurador-cfg.js').config;
var MongoPersistence = require('../apurador/MongoPersistence').MongoPersistence;
var async = require('async');

logger.remove(winston.transports.Console);
logger.add(winston.transports.Console, {timestamp: true, colorize: true});

var batchNumber = process.argv[3];
var step = parseInt(process.argv[4]);

var mongo = new MongoPersistence(config);
mongo.init(function () {}, function () {});

app.use('/comum', express.static(__dirname + '/../test-data/comum'));

app.use(bodyParser.json());

app.post('/change-source', function (req, res) {
	var body = req.body;
	logger.info('Changing source to batch', body.batch + ', step', body.step);
	batchNumber = body.batch;
	step = body.step;
	res.status(200).end();
})

app.post('/reset-db', function (req, res) {
	logger.info('Reseting database')
	async.series([
			function (cb) {
				mongo.dropCollection('main_ufs_indice', cb);
			},
			function (cb) {
				mongo.dropCollection('main_ufs_fixoAbrangencia', cb);
			}
		],
		function (err) {
			if (err) {
				logger.error('Failed to reset database', err.stack || err);
				res.status(500).end('Failed to reset database' + err.stack || err)
			} else {
				logger.info('Database successfully reset')
				res.status(200).end('Database successfully reset');
			}
		});
})

app.get('/*', function (req, res) {
	var baseDir = path.join(__dirname, '../test-data/batch-' + batchNumber);

	var curStep = step;

	while (curStep > 0) {
		var strStep = ("00" + curStep).slice(-2);
		var file = path.join(baseDir, strStep, req.path);
		var stat;

		try {
			stat = fs.statSync(file)	
		} catch (e) {
			logger.debug('[NOT FOUND] File not found at', file, '- searching on previous step...');
			curStep--;
			continue;
		}

		if (stat.isFile()) {
			logger.info('[FOUND] File found at', file);
			res.sendFile(file);
			return;
		} else {
			logger.debug('[INVALID] File not suitable to access:', file)
			curStep--;
			continue;
		}
		
		curStep--;
	}

	logger.info('[NOT FOUND]', file);
	res.status(404);
	res.end();
})

app.listen(8080);

logger.info('HTTP server started')
